---
layout:           post
title:            'Party tracker: v1.0.0'
date:             2016-08-07 22:20
version:          1.0.0
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       1.0.0
      url:        dnd-party-tracker-v1.0.0.pdf
---

My own version that I'll use for my own homebrew campaign. Supports a total of 5 players and the ability to track the group's advancement. Able to reset the page or individual players.

### Added:
- Group tracking:
  - Group name
  - Group Reputation
  - Current group XP
  - Current group Gold
- Player tracking:
  - Player name
  - Character name
  - Class
  - Level
  - Race
  - Max HP
  - Current HP
  - Armor Class
  - Passive Wisdom
  - Speed
  - Character reputation
  - Languages
  - Magic Items
  - Notes
