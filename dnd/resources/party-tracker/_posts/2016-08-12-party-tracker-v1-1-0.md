---
layout:           post
title:            'Party tracker: v1.1.0'
date:             2016-08-12 22:43
version:          1.1.0
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       1.1.0
      url:        dnd-party-tracker-v1.1.0.pdf
---

### Fixed:
- Font changed to [Ubuntu](http://font.ubuntu.com/) (last font change, I promise!).
