---
layout:           post
title:            'Party tracker: v1.0.1'
date:             2016-08-12 00:53
version:          1.0.1
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       1.0.1
      url:        dnd-party-tracker-v1.0.1.pdf
---

### Fixed:
- Centered "Campaign Name" text.
- Changed all "Reputation" fields to accept any text (not just numbers).
- Corrected position of "Languages & Magic Items" fields.
- "Passive Wisdom" fields have a smaller text now (not sure how it will look when printing, feedback is welcomed!).
- Corrected Tab order (they changed because I moved "Languages & Magic Items"-fields).
