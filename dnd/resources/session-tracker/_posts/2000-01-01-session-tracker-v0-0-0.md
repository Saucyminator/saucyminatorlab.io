---
layout:           post
title:            'Session tracker: v1.0.0'
date:             2016-08-13 16:56
version:          1.0.0
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       1.0.0
      url:        dnd-session-tracker-v1.0.0.pdf
---

My own version of keeping track of a session (I've only had one session per pdf in mind when I made this). XP, Gold, Magic Items, Reputation and some handy notes!

### Added:
- Font is [Ubuntu](http://font.ubuntu.com/).
- Session #.
- Date (both in-game & date played the session).
- Location (a place to write down where you played the session).
- "Time of day" tracking.
- XP, Gold, Magic Items and Reputation.
- Notes (keep track of EVERYTHING that happened during the session!)
