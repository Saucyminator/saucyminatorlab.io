---
layout:           post
title:            'Character inventory: v1.0.1'
date:             2016-06-16 16:19
version:          1.0.1
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       Male version
      url:        dnd-character-inventory-male-v1.0.1.pdf
    - name:       Female version
      url:        dnd-character-inventory-female-v1.0.1.pdf
---

### Fixed:
- Corrected position of "Rings (left hand)"-text.
