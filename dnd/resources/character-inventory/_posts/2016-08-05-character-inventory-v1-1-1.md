---
layout:           post
title:            'Character inventory: v1.1.1'
date:             2016-08-05 19:13
version:          1.1.1
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       Male version
      url:        dnd-character-inventory-male-v1.1.1.pdf
    - name:       Female version
      url:        dnd-character-inventory-female-v1.1.1.pdf
---

### Added:
- Added Author and credits to the PDF properties.
- Font changed to Segoe UI.

### Fixed:
- Centered text in all fields.
- Repositioned & resized every field.
