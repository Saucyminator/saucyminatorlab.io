---
layout:           post
title:            'Character inventory: v1.1.0'
date:             2016-07-25 22:16
version:          1.1.0
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       Male version
      url:        dnd-character-inventory-male-v1.1.0.pdf
    - name:       Female version
      url:        dnd-character-inventory-female-v1.1.0.pdf
---

### Added:
- Tooltip for all fields.
- Checkbox for items that are attuned (NOTE: Make sure highlight fields is enabled to see them. Or you could use an asterisk in the title of the item if you so choose).
