---
layout:           post
title:            'Character inventory: v1.0.0'
date:             2016-06-15 00:46
version:          1.0.0
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       Male version
      url:        dnd-character-inventory-male-v1.0.0.pdf
    - name:       Female version
      url:        dnd-character-inventory-female-v1.0.0.pdf
---

### Added:
- First iteration of player inventory.
- Male and female versions included.
- Included inventory slots:
  - Head: _Circlets, crowns, hats, headbands, helmets._
  - Face: _Goggles, visors, lenses, masks, third eyes._
  - Neck: _Amulets, badgets, medallions, pendants, scarfs._
  - Shoulders: _Capes, cloaks, mantles, shawls._
  - Body: _Armor, robes._
  - Torso: _Shirts, tunics, vests, vestments._
  - Arms: _Armbands, bracelets, bracers._
  - Hands: _Gauntlets, gloves._
  - Waist: _Belts, girdles, sashes._
  - Legs: _Greaves, cuisses, tassets._
  - Feet: _Boots, sandals, shoes, slippers._
  - Weapons: _Weapons, Staffs, Rods, Wands, Shields._
  - Rings _(left hand)._
  - Rings _(right hand)._
- Sources
  - [Inspiration sheet](http://epiclevelgaming.com/dd-5e-alternate-character-sheets/).
  - [Item box layout: Curse of Strahd character sheet](http://dnd.wizards.com/articles/events/adventurers-league-resources).
  - [Male image](http://www.dreamstime.com/stock-illustration-warrior-man-silhouette-stylized-walking-fantasy-armor-pulling-out-sword-his-back-available-vector-eps-format-image50079533).
  - [Female image](http://www.dreamstime.com/stock-illustration-knight-woman-silhouette-stylized-walking-warrior-sword-fantasy-armor-available-vector-eps-format-image41618907).
