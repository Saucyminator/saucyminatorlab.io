---
layout:           post
title:            'Character sheet: v1.2.0'
date:             2015-10-17 12:48
version:          1.2.0
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       1.2.0
      url:        dnd-character-sheet-v1.2.0.pdf
  extra:          https://gitlab.com/Saucyminator/saucyminator.gitlab.io/commit/19445cd2ef61fdb7d7754b5ec4e81786cd6bee26
---

### Fixed:
- Cleanup of field names.
