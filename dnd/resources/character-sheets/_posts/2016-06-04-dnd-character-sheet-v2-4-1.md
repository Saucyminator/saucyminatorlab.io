---
layout:           post
title:            'Character sheet: v2.4.1'
date:             2016-06-04 19:29
version:          2.4.1
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       2.4.1
      url:        dnd-character-sheet-v2.4.1.pdf
  extra:          
---

### Fixed:
- Tracker boxes to accept multi-line text.
  - Also centered its text.
