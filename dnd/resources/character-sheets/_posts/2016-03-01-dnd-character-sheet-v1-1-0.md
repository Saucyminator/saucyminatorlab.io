---
layout:           post
title:            'Character sheet: v1.1.0'
date:             2015-10-17 12:39
version:          1.1.0
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       1.1.0
      url:        dnd-character-sheet-v1.1.0.pdf
  extra:          https://gitlab.com/Saucyminator/saucyminator.gitlab.io/commit/a1fbd97137c5262889d77a2221bea30e35c9d05f
---

_NOTE: v1.0.0 of my character-sheet is lost._

### Added:
- First iteration of calculations.
