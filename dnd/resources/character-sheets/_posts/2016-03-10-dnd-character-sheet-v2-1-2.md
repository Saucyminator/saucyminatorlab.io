---
layout:           post
title:            'Character sheet: v2.1.2'
date:             2016-03-10 23:40
version:          2.1.2
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       2.1.2
      url:        dnd-character-sheet-v2.1.2.pdf
  extra:          https://gitlab.com/Saucyminator/saucyminator.gitlab.io/commit/b32e8a94eda77c8135b8c83b3d853a0647c2129e
---

### Fixed:
- Forgot to update switch case for the Drow race (see JavaScript changes).
