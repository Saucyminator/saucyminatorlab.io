---
layout:           post
title:            'Character sheet: v3.0.2'
date:             2016-08-12 16:50
version:          3.0.2
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       3.0.2
      url:        dnd-character-sheet-v3.0.2.pdf
  extra:          
---

### Fixed:
- Tab order of fields (always gets messed up if I move any fields).
