---
layout:           post
title:            'Character sheet: v2.0.0'
date:             2015-10-24 12:01
version:          2.0.0
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       2.0.0
      url:        dnd-character-sheet-v2.0.0.pdf
  extra:          https://gitlab.com/Saucyminator/saucyminator.gitlab.io/commit/1954bcb2d678ed390305b02b0238450c398bf9cd
---

### Added:
- Class dropdown-list:
  - "Saving throws" automatically added.
  - Rogues: Can choose skills to be their Expertise skill (checkbox is hidden when printing).
  - "Spellcasting class" automatically added based on the chosen class.
- Background dropdown-list.
- Race dropdown-list:
  - "Hit Dice total" uses the total character level.
  - "Hit Dice" automatically added based on the player's race, adds the player's CON-Mod to the calculation.
- "Proficiency Bonus" calculated based on Level.
- "Alert" feat checkbox (adds +5 to Initiative).

### Fixed:
- "Alignment" text size.
- "Passive Wisdom" calculation.
- Split "Class" & "Level".
- Split Ability Scores into:
  - Stats added/rolled by the player (field is hidden when printing).
  - Stats added by the chosen race (field is hidden when printing).
  - Stats by the player & race combined into a single one (shown when printing, it's the small number at the top of the Ability Score box).
  - Ability Modifier automatically calculated based on the combined value.
- Initiative calculated based on DEX-Mod.
