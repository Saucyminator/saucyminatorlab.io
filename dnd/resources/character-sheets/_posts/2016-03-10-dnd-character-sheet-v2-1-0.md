---
layout:           post
title:            'Character sheet: v2.1.0'
date:             2016-03-10 21:47
version:          2.1.0
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       2.1.0
      url:        dnd-character-sheet-v2.1.0.pdf
  extra:          https://gitlab.com/Saucyminator/saucyminator.gitlab.io/commit/e6d0feeeb69b9398654e623288855ba691da342c
---

### Added:
- Multiclass support.

### Fixed:
- Sorted race dropdown-list.
- "Proficiency Bonus" calculated correctly when multiclassing.
