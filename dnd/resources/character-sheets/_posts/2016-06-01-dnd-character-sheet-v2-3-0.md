---
layout:           post
title:            'Character sheet: v2.3.0'
date:             2016-06-01 13:45
version:          2.3.0
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       2.3.0
      url:        dnd-character-sheet-v2.3.0.pdf
  extra:          
---

### Added:
- Text fields for player and character names (located at the top right of the first page).
