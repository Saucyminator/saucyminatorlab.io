---
layout:           post
title:            'Character sheet: v2.4.2'
date:             2016-06-04 20:07
version:          2.4.2
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       2.4.2
      url:        dnd-character-sheet-v2.4.2.pdf
  extra:          
---

### Fixed:
- Centered text in the gold fields.
- Removed scroll-text property from most text fields.
