---
layout:           post
title:            'Character sheet: v2.2.0'
date:             2016-06-01 12:46
version:          2.2.0
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       2.2.0
      url:        dnd-character-sheet-v2.2.0.pdf
  extra:          https://gitlab.com/Saucyminator/saucyminator.gitlab.io/commit/5e7f81bc77bb6c0980a92e930fa20e51f7e15f45
---

### Added:
- "Human Variant Trait" checkbox (only shown if you choose the Human race).
  - Use this if your DM allow you to use *Human Variant Trait* option, an unchecked checkbox adds the +1 to all Ability Scores.
