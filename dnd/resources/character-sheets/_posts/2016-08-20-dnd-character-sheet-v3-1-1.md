---
layout:           post
title:            'Character sheet: v3.1.1'
date:             2016-08-20 16:26
version:          3.1.1
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       3.1.1
      url:        dnd-character-sheet-v3.1.1.pdf
  extra:          
---

### Fixed:
- Corrected copyright text on the second page.
