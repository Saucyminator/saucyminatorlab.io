---
layout:           post
title:            'Character sheet: v2.5.0'
date:             2016-07-24 18:22
version:          2.5.0
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       2.5.0
      url:        dnd-character-sheet-v2.5.0.pdf
  extra:          
---

### Added:
- Ability Score improvement (+1) checkbox beside all Ability Scores, for campaigns that use the *Variant Human Trait* for Humans.
