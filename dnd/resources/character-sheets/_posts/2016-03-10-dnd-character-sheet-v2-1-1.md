---
layout:           post
title:            'Character sheet: v2.1.1'
date:             2016-03-10 23:18
version:          2.1.1
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       2.1.1
      url:        dnd-character-sheet-v2.1.1.pdf
  extra:          
---

### Fixed:
- Reset default settings.
