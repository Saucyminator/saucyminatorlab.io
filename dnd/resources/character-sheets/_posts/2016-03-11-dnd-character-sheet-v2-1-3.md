---
layout:           post
title:            'Character sheet: v2.1.3'
date:             2016-03-11 00:24
version:          2.1.3
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       2.1.3
      url:        dnd-character-sheet-v2.1.3.pdf
  extra:          https://gitlab.com/Saucyminator/saucyminator.gitlab.io/commit/3b0e656a0719b738332191b056667b28d0fad9de
---

### Fixed:
- Multiclass "Hit Dice" calculations.
