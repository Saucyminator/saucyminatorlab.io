---
layout:           post
title:            'Character sheet: v2.4.0'
date:             2016-06-03 23:24
version:          2.4.0
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       2.4.0
      url:        dnd-character-sheet-v2.4.0.pdf
  extra:          
---

### Added:
- Tracker boxes for arrows, lockpicks, misc stuff, etc.
  - Located down by the equipment & character notes box on the first page.

### Fixed:
- Evenly spaced out "Player & Character names"-fields.
