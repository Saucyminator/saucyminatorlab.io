---
layout:           post
title:            'Character sheet: v2.2.1'
date:             2016-06-01 13:12
version:          2.2.1
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       2.2.1
      url:        dnd-character-sheet-v2.2.1.pdf
  extra:          
---

_NOTE: This should've been v2.3.0 - because it's an addition not a bugfix. Oh well._

### Added:
- Character sheet version (might be good to know).
  - Located underneath "Dungeons & Dragons"-text at the top of the first page.
