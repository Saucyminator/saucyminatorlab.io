---
layout:           post
title:            'Character sheet: v3.0.0'
date:             2016-08-06 01:29
version:          3.0.0
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       3.0.0
      url:        dnd-character-sheet-v3.0.0.pdf
  extra:          
---

### Added:
- Races added from Player's Companion: Arakocra, Deep Gnome, Genasi (Air, Earth, Fire and Water) and Goliath.
- "Spellcasting Ability", "Spell Save DC" and "Spell Attack Bonus" on the first page (under Hit Points).
- Changed font to Segoe UI.

### Fixed:
- Corrected tab order of "Player & Character Names"-fields.
- All spell field names and tab order (it took a while zzZZzz...).
- "XP"-field is now a numbers-only field that has separators (commas).
- Re-enabled "Inspiration" field (was read-only).
- Changed layout of Maximum/Temporary/Current HP into small squares (I find it just a bit better to keep track of).
- Cleaned up Abilities (Athletics, Acrobatics, etc) checkboxes and text. Now much neater. Neat!
