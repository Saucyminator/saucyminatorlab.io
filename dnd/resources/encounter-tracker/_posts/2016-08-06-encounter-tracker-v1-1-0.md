---
layout:           post
title:            'Encounter tracker: v1.1.0'
date:             2016-08-06 21:34
version:          1.1.0
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       v1.1.0
      url:        dnd-encounter-tracker-v1.1.0.pdf
  extra:          
---

### Added:
- Automatic Initiative calculation based on Dexterity modifier.

### Fixed:
- "Attacks & Spellcasting" and "Features & Skills" fields position and height, can now hold a few more lines of text!
