---
layout:           post
title:            'Encounter tracker: v1.0.1'
date:             2016-08-06 04:54
version:          1.0.1
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       v1.0.1
      url:        dnd-encounter-tracker-v1.0.1.pdf
  extra:          
---

### Fixed:
- Corrected tab order of the first monster HP.
