---
layout:           post
title:            'Encounter tracker: v1.3.0'
date:             2016-08-12 18:10
version:          1.3.0
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       v1.3.0
      url:        dnd-encounter-tracker-v1.3.0.pdf
  extra:          
---

### Added:
- Ability to reset the whole sheet, Initiative or individual monsters.
