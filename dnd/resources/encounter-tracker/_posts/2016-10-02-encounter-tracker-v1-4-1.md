---
layout:           post
title:            'Encounter tracker: v1.4.1'
date:             2016-10-02 13:04
version:          1.4.1
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       v1.4.1
      url:        dnd-encounter-tracker-v1.4.1.pdf
  extra:          
---

### Fixed:
- Corrected Challenge rating (1/8, 1/4 and 1/2) calculation.
