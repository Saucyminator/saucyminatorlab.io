---
layout:           post
title:            'Encounter tracker: v1.0.0'
date:             2016-08-05 17:39
version:          1.0.0
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       v1.0.0
      url:        dnd-encounter-tracker-v1.0.0.pdf
  extra:          
---

My version of an encounter tracker, based on the 5th Edition Combat Tracker made by Neskatomonin [[Source](http://www.reddit.com/r/DnD/comments/2d0y02/5th_edition_combat_tracker/)].

This version includes the following:

### Added:
- Change font to Segoe UI.
- Added a "Version" field (read only; updated only by me for each new version) which uses [SemVer](http://semver.org/).
- Less "Initiative" fields, now only counting from 1-10.
- "Notes" field are much bigger.
- "Campaign Name"-field.
- Added date fields:
  - "Date Created", a field you could fill in when you created the encounter (could be cool to have if you want to look through your previous encounters a few years from now).
  - "Date Played", a field when you used/played the encounter in your campaign.
- Automatic calculation of monster modifiers.
- Added "Alignment" dropdown-list.
- Changed art for "Initiative" and "Notes" fields. (Adobe Acrobat ain't easy to work with. There's no way of aligning art with other art other than using eye measurements, I would rather use Photoshop).

### Fixed:
- Correctly named every field to ensure no duplicate data.
- Changed many fields' size to better fit the new font (and easier to read).
