---
layout:           post
title:            'Encounter tracker: v1.1.1'
date:             2016-08-08 01:56
version:          1.1.1
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       v1.1.1
      url:        dnd-encounter-tracker-v1.1.1.pdf
  extra:          
---

### Fixed:
- Allowed Rich Text to the "Notes", "Attacks & Spellcasting" and "Features & Skills" fields.
