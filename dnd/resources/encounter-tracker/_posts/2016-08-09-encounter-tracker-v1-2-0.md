---
layout:           post
title:            'Encounter tracker: v1.2.0'
date:             2016-08-09 19:11
version:          1.2.0
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       v1.2.0
      url:        dnd-encounter-tracker-v1.2.0.pdf
  extra:          https://gitlab.com/Saucyminator/saucyminator.gitlab.io/commit/42b8c78deda6e63a26919754db51b83d5d1e4a7c
---

### Added:
- JavaScript file that includes all JavaScript used in the PDF.

### Fixed:
- Monster "XP" is now automatically calculated based on Challenge Rating (0, 1/8, 1/4, 1/2, and 1 to 30).
- Monster Size is now a dropdown-list that uses the available sizes from the Monster Manual (but text is customizable).
