---
layout:           post
title:            Space Evader
date:             2016-01-12 05:03
date_updated:     
tags:             [GameDev, Games, Unity, C#, Prototype, Bullet hell, Singleplayer]
summary:          Simple game where the player needs to evade lasers shot by an alien mothership.
images:
  banner:         https://dev.saucy.se/unity/space-evader/banner.png # 1920x540
  thumbnail:      # 384x216
  screenshot:     # 1920x1080
  screenshots:    # thumbnail: 384x216, screenshot: 1920x1080
    - thumbnail:  https://dev.saucy.se/unity/space-evader/first_prototype.gif
      screenshot: https://dev.saucy.se/unity/space-evader/first_prototype.gif
    - thumbnail:  https://dev.saucy.se/unity/space-evader/progress_beams.gif
      screenshot: https://dev.saucy.se/unity/space-evader/progress_beams.gif
assets_separated: true # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       v0.2.0 win64 .zip file
      url:        https://dev.saucy.se/builds/space-evader/space-evader-v0.2.0-win64.zip
  source:         # external link or link to original source
---

Space Evader is the first prototype of a game I've created from scratch (a game that has a win/lose condition). Where the player has to evade the lasers shot from an alien mothership (the red thing at the top in the .gif). My main goal was to keep it simple (I always make things complicated, which ends up it not being completed)!

The game is coded in [C#](https://en.wikipedia.org/wiki/C_Sharp_(programming_language)) in the [Unity](https://unity3d.com/) game engine.
