---
layout:           post
title:            Break the Ice
date:             2015-08-19 12:00 # Got no exact date when I thought of this idea
date_updated:     
tags:             [GameDev, Games, Unreal Engine 4, Blueprint, Prototype, Top-Down, Singleplayer, Multi-directional shooter]
summary:          Shoot floating words to form a sentence.
images:
  banner:         https://dev.saucy.se/images/saucy.se/not-available/banner.png # 1920x540
  thumbnail:      # 384x216
  screenshot:     # 1920x1080
  screenshots:    # thumbnail: 384x216, screenshot: 1920x1080
assets_separated: true # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
  source:         # external link or link to original source
---

Inspired by [Asteroids](https://en.wikipedia.org/wiki/Asteroids_(video_game)) to create a game where the player shoots words to form sentences, and get scored based on how well put together the sentence is.

I didn't even code a prototype for the game, only wrote down a quick idea I had. I have no exact date when I thought of this idea either.
