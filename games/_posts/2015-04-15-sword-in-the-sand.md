---
layout:           post
title:            Sword in the Sand
date:             2015-04-15 12:00
date_updated:     
tags:             [GameDev, Games, Unreal Engine 4, Blueprint, Prototype, Top-Down, Singleplayer, Action-Adventure, RPG, Pixel]
summary:          A singleplayer RPG game. The player has a special sword and cloak, wakes up in the middle of the desert.
images:
  banner:         https://dev.saucy.se/images/saucy.se/not-available/banner.png # 1920x540
  thumbnail:      # 384x216
  screenshot:     # 1920x1080
  screenshots:    # thumbnail: 384x216, screenshot: 1920x1080
    - thumbnail:  https://dev.saucy.se/unity/sword-in-the-sand/player.png
      screenshot: https://dev.saucy.se/unity/sword-in-the-sand/player.png
assets_separated: true # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
  source:         # external link or link to original source
---

Sword in the Sand was a game (prototype) I had high aspirations of making, which I still hope to create or at least take some elements of in the future! It would be a singleplayer, pixel RPG game using a Top-Down camera (similar to [The Legend of Zelda: A Link to the Past](https://en.wikipedia.org/wiki/The_Legend_of_Zelda:_A_Link_to_the_Past)). I mostly focused on game design, concept and list of features than actually coding anything (how surprising).

Two sources of inspiration was [The Legend of Zelda: A Link to the Past](https://en.wikipedia.org/wiki/The_Legend_of_Zelda:_A_Link_to_the_Past) (my favorite game of all time) and a character concept created by Michael Doig ([source](http://michaeldoig.deviantart.com/art/Judas-Priest-Stars-Wars-369654937)). ![Judas Priest + Star Wars](http://pre07.deviantart.net/aa86/th/pre/f/2013/124/e/7/judas_priest___stars_wars___by_michaeldoig-d642zkp.png){:style="float: right; height: 500px; margin: 10px;"}

The setting was set in a very large desert, where the player plays a character called "Wanderer" (inspired by the character I've linked).

I had a long list of features I wanted to include in this game, things like:
- Level up character attributes (strength, dexterity, etc).
- Survival (gather food, water and find a shelter).
- Dynamic environments (sandstorms, moving sand dunes, etc).
- Upgradeable sword, which the game was focused around.
- Collect different types of cloaks, some are magical, others offer protection against the heat or the cold.
- Dynamic NPC (and questing) system, where rumors about the Player could spread to nearby village (good or bad).

The game is coded in [C#](https://en.wikipedia.org/wiki/C_Sharp_(programming_language)) in the [Unity](https://unity3d.com/) game engine.
