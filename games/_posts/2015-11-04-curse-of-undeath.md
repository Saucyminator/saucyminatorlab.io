---
layout:           post
title:            Curse of Undeath
date:             2015-11-04 13:00
date_updated:     2020-01-09 09:15
tags:             [GameDev, Games, Unreal Engine 4, Blueprint, Prototype, Sidescroller, Singleplayer, Pixel]
summary:          A sidescrolling game where the player is a skeleton that's been transported a long way from his grave, and all he wants to do is to go back home.
images:
  banner:         https://dev.saucy.se/unity/curse-of-undeath/banner.png # 1920x540
  thumbnail:      # 384x216
  screenshot:     # 1920x1080
  screenshots:    # thumbnail: 384x216, screenshot: 1920x1080
    - thumbnail:  https://dev.saucy.se/unity/curse-of-undeath/idle.gif
      screenshot: https://dev.saucy.se/unity/curse-of-undeath/idle.gif
    - thumbnail:  https://dev.saucy.se/unity/curse-of-undeath/walk.gif
      screenshot: https://dev.saucy.se/unity/curse-of-undeath/walk.gif
    - thumbnail:  https://dev.saucy.se/unity/curse-of-undeath/attack.gif
      screenshot: https://dev.saucy.se/unity/curse-of-undeath/attack.gif
    - thumbnail:  https://dev.saucy.se/unity/curse-of-undeath/font.png
      screenshot: https://dev.saucy.se/unity/curse-of-undeath/font.png
    - thumbnail:  https://dev.saucy.se/unity/curse-of-undeath/health.png
      screenshot: https://dev.saucy.se/unity/curse-of-undeath/health.png
    - thumbnail:  https://dev.saucy.se/unity/curse-of-undeath/help.png
      screenshot: https://dev.saucy.se/unity/curse-of-undeath/help.png
    - thumbnail:  https://dev.saucy.se/unity/curse-of-undeath/hotbar.png
      screenshot: https://dev.saucy.se/unity/curse-of-undeath/hotbar.png
    - thumbnail:  https://dev.saucy.se/unity/curse-of-undeath/mr_robot.png
      screenshot: https://dev.saucy.se/unity/curse-of-undeath/mr_robot.png
    - thumbnail:  https://dev.saucy.se/unity/curse-of-undeath/mockup-gameplay_v1_200118.png
      screenshot: https://dev.saucy.se/unity/curse-of-undeath/mockup-gameplay_v1_200118.png
    - thumbnail:  https://dev.saucy.se/unity/curse-of-undeath/attack-state_200313.gif
      screenshot: https://dev.saucy.se/unity/curse-of-undeath/attack-state_200313.gif
assets_separated: true # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
  source:         # external link or link to original source
---

I love games and movies where you can either play as the "bad guy" or the bad guys win. I mean, [Darth Vader](https://en.wikipedia.org/wiki/Darth_Vader) is pretty badass right? I also love the Undead, Zombies, etc, ([Necrons](http://warhammer40k.wikia.com/wiki/Necrons) in Warhammer 40k was one of my favorite races growing up - they pretty much remind me of Undead creatures).

So I wanted to create a game where you play as the "bad guy", where the player fights against heroes and townsfolk to get make their way back home to the grave.

The original game was "coded" using Blueprint in [Unreal Engine 4](https://www.unrealengine.com/what-is-unreal-engine-4) game engine.

**Update 2020**  
The updated version of this game was remade into a 2D local multiplayer CO-OP game.
