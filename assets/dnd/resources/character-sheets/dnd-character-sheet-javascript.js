//-------------------------------------------------------------
//-----------------Do not edit the XML tags--------------------
//-------------------------------------------------------------

//<Document-Level>
//<ACRO_source>classHitDice</ACRO_source>
//<ACRO_script>
/*********** belongs to: Document-Level:classHitDice ***********/
function classHitDice (cla) {
  switch (cla) {
    case 'Barbarian':
      return '1d12';
      break;
    case 'Fighter':
    case 'Paladin':
    case 'Ranger':
      return '1d10';
      break;
    case 'Bard':
    case 'Cleric':
    case 'Druid':
    case 'Monk':
    case 'Rogue':
    case 'Warlock':
      return '1d8';
      break;
    case 'Sorcerer':
    case 'Wizard':
      return '1d6';
      break;
    default:
      break;
  }
}
//</ACRO_script>
//</Document-Level>

//<Document-Level>
//<ACRO_source>displayOptions</ACRO_source>
//<ACRO_script>
/*********** belongs to: Document-Level:displayOptions ***********/
function displayOptions (element, option) {
  switch (option) {
    case 'visible':
      // Visible
      element.display = display.visible;
      break;
    case 'hidden':
      // Hidden
      element.display = display.hidden;
      break;
    case 'noprint':
      // Visible but doesn't print
      element.display = display.noPrint;
      break;
    case 'printhidden':
      // Hidden but does print
      element.display = display.noView;
      break;
    default:
      break;
  }
}
//</ACRO_script>
//</Document-Level>

//<Document-Level>
//<ACRO_source>formatScore</ACRO_source>
//<ACRO_script>
/*********** belongs to: Document-Level:formatScore ***********/
function formatScore (score) {
  if (score > 0) {
    return "+" + score;
  } else {
    return score;
  }
}
//</ACRO_script>
//</Document-Level>

//<AcroForm>
//<ACRO_source>AbilityCHAMod:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:AbilityCHAMod:Calculate ***********/
var score = formatScore(Math.floor((this.getField("AbilityCHA").value - 10) / 2));

event.value = score;

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>AbilityCONMod:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:AbilityCONMod:Calculate ***********/
var score = formatScore(Math.floor((this.getField("AbilityCON").value - 10) / 2));

event.value = score;

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>AbilityDEXMod:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:AbilityDEXMod:Calculate ***********/
var score = formatScore(Math.floor((this.getField("AbilityDEX").value - 10) / 2));

event.value = score;

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>AbilityINTMod:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:AbilityINTMod:Calculate ***********/
var score = formatScore(Math.floor((this.getField("AbilityINT").value - 10) / 2));

event.value = score;

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>AbilitySTRMod:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:AbilitySTRMod:Calculate ***********/
var score = formatScore(Math.floor((this.getField("AbilitySTR").value - 10) / 2));

event.value = score;

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>AbilityWISMod:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:AbilityWISMod:Calculate ***********/
var score = formatScore(Math.floor((this.getField("AbilityWIS").value - 10) / 2));

event.value = score;

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>CharacterName2:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:CharacterName2:Calculate ***********/
event.value = this.getField("CharacterName").value;

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>Class:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:Class:Calculate ***********/
var Class       = this.getField('Class').value;
var Proficiency = this.getField('ProficiencyBonus').value;
var STR         = this.getField('AbilitySTRSavingThrows');
var STRMod      = this.getField('AbilitySTRMod').value;
var STRCheckbox = this.getField('AbilitySTRSavingThrowsCheckbox');
var DEX         = this.getField('AbilityDEXSavingThrows');
var DEXMod      = this.getField('AbilityDEXMod').value;
var DEXCheckbox = this.getField('AbilityDEXSavingThrowsCheckbox');
var CON         = this.getField('AbilityCONSavingThrows');
var CONMod      = this.getField('AbilityCONMod').value;
var CONCheckbox = this.getField('AbilityCONSavingThrowsCheckbox');
var INT         = this.getField('AbilityINTSavingThrows');
var INTMod      = this.getField('AbilityINTMod').value;
var INTCheckbox = this.getField('AbilityINTSavingThrowsCheckbox');
var WIS         = this.getField('AbilityWISSavingThrows');
var WISMod      = this.getField('AbilityWISMod').value;
var WISCheckbox = this.getField('AbilityWISSavingThrowsCheckbox');
var CHA         = this.getField('AbilityCHASavingThrows');
var CHAMod      = this.getField('AbilityCHAMod').value;
var CHACheckbox = this.getField('AbilityCHASavingThrowsCheckbox');

STRCheckbox.value = 'Off';
DEXCheckbox.value = 'Off';
CONCheckbox.value = 'Off';
INTCheckbox.value = 'Off';
WISCheckbox.value = 'Off';
CHACheckbox.value = 'Off';

STR.value = formatScore(STRMod);
DEX.value = formatScore(DEXMod);
CON.value = formatScore(CONMod);
INT.value = formatScore(INTMod);
WIS.value = formatScore(WISMod);
CHA.value = formatScore(CHAMod);

switch (Class) {
  case 'Barbarian':
    STRCheckbox.value = 'Yes';
    CONCheckbox.value = 'Yes';
    STR.value         = formatScore(STRMod + Proficiency);
    CON.value         = formatScore(CONMod + Proficiency);
    break;
  case 'Bard':
    DEXCheckbox.value = 'Yes';
    CHACheckbox.value = 'Yes';
    DEX.value         = formatScore(DEXMod + Proficiency);
    CHA.value         = formatScore(CHAMod + Proficiency);
    break;
  case 'Cleric':
    WISCheckbox.value = 'Yes';
    CHACheckbox.value = 'Yes';
    WIS.value         = formatScore(WISMod + Proficiency);
    CHA.value         = formatScore(CHAMod + Proficiency);
    break;
  case 'Druid':
    INTCheckbox.value = 'Yes';
    WISCheckbox.value = 'Yes';
    INT.value         = formatScore(INTMod + Proficiency);
    WIS.value         = formatScore(WISMod + Proficiency);
    break;
  case 'Fighter':
    STRCheckbox.value = 'Yes';
    CONCheckbox.value = 'Yes';
    STR.value         = formatScore(STRMod + Proficiency);
    CON.value         = formatScore(CONMod + Proficiency);
    break;
  case 'Monk':
    STRCheckbox.value = 'Yes';
    DEXCheckbox.value = 'Yes';
    STR.value         = formatScore(STRMod + Proficiency);
    DEX.value         = formatScore(DEXMod + Proficiency);
    break;
  case 'Paladin':
    WISCheckbox.value = 'Yes';
    CHACheckbox.value = 'Yes';
    WIS.value         = formatScore(WISMod + Proficiency);
    CHA.value         = formatScore(CHAMod + Proficiency);
    break;
  case 'Ranger':
    STRCheckbox.value = 'Yes';
    DEXCheckbox.value = 'Yes';
    STR.value         = formatScore(STRMod + Proficiency);
    DEX.value         = formatScore(DEXMod + Proficiency);
    break;
  case 'Rogue':
    DEXCheckbox.value = 'Yes';
    INTCheckbox.value = 'Yes';
    DEX.value         = formatScore(DEXMod + Proficiency);
    INT.value         = formatScore(INTMod + Proficiency);
    break;
  case 'Sorcerer':
    CONCheckbox.value = 'Yes';
    CHACheckbox.value = 'Yes';
    CON.value         = formatScore(CONMod + Proficiency);
    CHA.value         = formatScore(CHAMod + Proficiency);
    break;
  case 'Warlock':
    WISCheckbox.value = 'Yes';
    CHACheckbox.value = 'Yes';
    WIS.value         = formatScore(WISMod + Proficiency);
    CHA.value         = formatScore(CHAMod + Proficiency);
    break;
  case 'Wizard':
    INTCheckbox.value = 'Yes';
    WISCheckbox.value = 'Yes';
    INT.value         = formatScore(INTMod + Proficiency);
    WIS.value         = formatScore(WISMod + Proficiency);
    break;
  default:
    break;
}

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>Faction Symbol Image:Annot1:MouseUp:Action1</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:Faction Symbol Image:Annot1:MouseUp:Action1 ***********/
// Prompt user to select file to display in this button

event.target.buttonImportIcon();
//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>HitDice:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:HitDice:Calculate ***********/
var Class      = this.getField("Class").value;
var ClassMulti = this.getField("MultiClass").value;
var CONMod     = this.getField("AbilityCONMod").value;

function addConMod (hitDice) {
  if (CONMod != 0) {
    return hitDice + formatScore(CONMod);
  } else {
    return hitDice;
  }
}

var result;

if (ClassMulti == '' || ClassMulti == ' ' || ClassMulti == Class) {
  result = addConMod(classHitDice(Class));
} else {
  result = addConMod(classHitDice(Class)) + ', ' + addConMod(classHitDice(ClassMulti));
}

event.value = result;

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>HitDiceTotal:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:HitDiceTotal:Calculate ***********/
var level      = this.getField("Level").value;
var multiLevel = this.getField("MultiClassLevel").value;

var result;

if (multiLevel) {
  result = level + multiLevel;
} else {
  result = level;
}

event.value = result;
//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>Image:Annot1:MouseUp:Action1</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:Image:Annot1:MouseUp:Action1 ***********/
// Prompt user to select file to display in this button

event.target.buttonImportIcon();
//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>Initiative:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:Initiative:Calculate ***********/
var Class             = this.getField('Class').value;
var Proficiency       = this.getField('ProficiencyBonus').value;
var JackOfAllTrades   = this.getField('SkillJackOfAllTradesCheckbox').value;
var RemarkableAthlete = this.getField('SkillRemarkableAthleteCheckbox').value;
var AlertCheckbox     = this.getField('FeatAlertCheckbox').value;
var DEX               = this.getField("AbilityDEXMod").value;
var Score             = 0;

if (JackOfAllTrades == 'Yes' && Class == 'Bard') {
  Score = DEX + Math.floor(Proficiency / 2);
} else if (RemarkableAthlete == 'Yes' && Class == 'Fighter') {
  Score = DEX + Math.ceil(Proficiency / 2);
} else {
  Score = DEX;
}

if (AlertCheckbox == 'Yes') {
  event.value = formatScore(Score + 5);
} else {
  event.value = formatScore(Score);
}

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>PassiveWisdom:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:PassiveWisdom:Calculate ***********/
var Class       = this.getField('Class').value;
var Proficiency = this.getField('ProficiencyBonus').value;
var WISMod      = this.getField('AbilityWISMod').value;
var Checkbox    = this.getField('SkillPerceptionCheckbox').value;
var Expertise   = this.getField('SkillPerceptionExpertiseCheckbox').value;
var score       = 10 + WISMod;

if (Checkbox == 'Yes') {
  if (Class == 'Rogue' && Expertise == 'Yes') {
    score = score + (Proficiency * 2);
  } else {
    score = score + Proficiency;
  }
}

event.value = score;

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>PreparedSpells:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:PreparedSpells:Calculate ***********/
var Class  = this.getField('Class').value;
var Self   = this.getField('PreparedSpells');
var Level  = this.getField('Level').value;
var WISMod = this.getField('AbilityWISMod').value;

if (Class == 'Cleric') {
  Self.display = display.visible;
} else {
  Self.display = display.hidden;
}

var PreparedSpells = Level + WISMod;

event.value = '# available prepared spells: ' + PreparedSpells;

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>ProficiencyBonus:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:ProficiencyBonus:Calculate ***********/
var level      = this.getField("Level").value;
var multiLevel = this.getField("MultiClassLevel").value;
var score      = Math.floor(((level + multiLevel) - 1) / 4 + 2);
event.value    = formatScore(score);
//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>Race:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:Race:Calculate ***********/
var Self                = event.value;
var STR                 = this.getField('AbilitySTRRace');
var DEX                 = this.getField('AbilityDEXRace');
var CON                 = this.getField('AbilityCONRace');
var INT                 = this.getField('AbilityINTRace');
var WIS                 = this.getField('AbilityWISRace');
var CHA                 = this.getField('AbilityCHARace');
var HumanCheckbox       = this.getField('VariantHumanTraitCheckbox').value;
var AbilityStrength     = this.getField('AbilityScoreImprovementStrengthCheckbox');
var AbilityDexterity    = this.getField('AbilityScoreImprovementDexterityCheckbox');
var AbilityConstitution = this.getField('AbilityScoreImprovementConstitutionCheckbox');
var AbilityIntelligence = this.getField('AbilityScoreImprovementIntelligenceCheckbox');
var AbilityWisdom       = this.getField('AbilityScoreImprovementWisdomCheckbox');
var AbilityCharisma     = this.getField('AbilityScoreImprovementCharismaCheckbox');

STR.value = 0;
DEX.value = 0;
CON.value = 0;
INT.value = 0;
WIS.value = 0;
CHA.value = 0;

function showTraits () {
  displayOptions(AbilityStrength, 'noprint');
  displayOptions(AbilityDexterity, 'noprint');
  displayOptions(AbilityConstitution, 'noprint');
  displayOptions(AbilityIntelligence, 'noprint');
  displayOptions(AbilityWisdom, 'noprint');
  displayOptions(AbilityCharisma, 'noprint');
}

function hideTraits () {
  displayOptions(AbilityStrength, 'hidden');
  displayOptions(AbilityDexterity, 'hidden');
  displayOptions(AbilityConstitution, 'hidden');
  displayOptions(AbilityIntelligence, 'hidden');
  displayOptions(AbilityWisdom, 'hidden');
  displayOptions(AbilityCharisma, 'hidden');
}

switch (Self) {
  case 'Aarakocra':
    DEX.value = 2;
    WIS.value = 1;
    break;
  case 'Deep Gnome':
    DEX.value = 1;
    INT.value = 2;
    break;
  case 'Dragonborn':
    STR.value = 2;
    CHA.value = 1;
    break;
  case 'Dwarf (Hill)':
    CON.value = 2;
    WIS.value = 1;
    break;
  case 'Dwarf (Mountain)':
    STR.value = 2;
    CON.value = 2;
    break;
  case 'Elf (Drow)':
    DEX.value = 2;
    CHA.value = 1;
    break;
  case 'Elf (High)':
    DEX.value = 2;
    INT.value = 1;
    break;
  case 'Elf (Wood)':
    DEX.value = 2;
    WIS.value = 1;
    break;
  case 'Genasi (Air)':
    DEX.value = 1;
    CON.value = 2;
    break;
  case 'Genasi (Earth)':
    STR.value = 1;
    CON.value = 2;
    break;
  case 'Genasi (Fire)':
    CON.value = 2;
    INT.value = 1;
    break;
  case 'Genasi (Water)':
    CON.value = 2;
    WIS.value = 1;
    break;
  case 'Gnome (Forest)':
    DEX.value = 1;
    INT.value = 2;
    break;
  case 'Gnome (Rock)':
    CON.value = 1;
    INT.value = 2;
    break;
  case 'Goliath':
    STR.value = 2;
    CON.value = 1;
    break;
  case 'Halfling (Lightfoot)':
    DEX.value = 2;
    CHA.value = 1;
    break;
  case 'Halfling (Stout)':
    DEX.value = 2;
    CON.value = 1;
    break;
  case 'Human':
    if (HumanCheckbox == 'Off') {
      hideTraits();

      STR.value = 1;
      DEX.value = 1;
      CON.value = 1;
      INT.value = 1;
      WIS.value = 1;
      CHA.value = 1;
    } else {
      showTraits();

      if (AbilityStrength.value == 'Yes') {
        STR.value = 1;
      }
      if (AbilityDexterity.value == 'Yes') {
        DEX.value = 1;
      }
      if (AbilityConstitution.value == 'Yes') {
        CON.value = 1;
      }
      if (AbilityIntelligence.value == 'Yes') {
        INT.value = 1;
      }
      if (AbilityWisdom.value == 'Yes') {
        WIS.value = 1;
      }
      if (AbilityCharisma.value == 'Yes') {
        CHA.value = 1;
      }
    }
    break;
  case 'Half-Elf':
    CHA.value = 2;

    showTraits();

    if (AbilityStrength.value == 'Yes') {
      STR.value = 1;
    }
    if (AbilityDexterity.value == 'Yes') {
      DEX.value = 1;
    }
    if (AbilityConstitution.value == 'Yes') {
      CON.value = 1;
    }
    if (AbilityIntelligence.value == 'Yes') {
      INT.value = 1;
    }
    if (AbilityWisdom.value == 'Yes') {
      WIS.value = 1;
    }
    if (AbilityCharisma.value == 'Yes') {
      CHA.value = 1;
    }
    break;
  case 'Half-Orc':
    STR.value = 2;
    CON.value = 1;
    break;
  case 'Tiefling':
    INT.value = 1;
    CHA.value = 2;
    break;
  default:
    break;
}

if (Self != 'Human' && Self != 'Half-Elf') {
  hideTraits();
}

STR.value = formatScore(STR.value);
DEX.value = formatScore(DEX.value);
CON.value = formatScore(CON.value);
INT.value = formatScore(INT.value);
WIS.value = formatScore(WIS.value);
CHA.value = formatScore(CHA.value);
//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>SkillAcrobatics:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:SkillAcrobatics:Calculate ***********/
var Mod               = this.getField('AbilityDEXMod').value;
var Checkbox          = this.getField('SkillAcrobaticsCheckbox').value;
var Expertise         = this.getField('SkillAcrobaticsExpertiseCheckbox');
var Class             = this.getField('Class').value;
var Proficiency       = this.getField('ProficiencyBonus').value;
var JackOfAllTrades   = this.getField('SkillJackOfAllTradesCheckbox').value;
var RemarkableAthlete = this.getField('SkillRemarkableAthleteCheckbox').value;

if (Checkbox == 'Yes' && Class == 'Rogue') {
  Expertise.display = display.noPrint;
} else {
  Expertise.display = display.hidden;
}

if (Checkbox == 'Yes') {
  if (Expertise.value == 'Yes' && Class == 'Rogue') {
    event.value = formatScore(Mod + (Proficiency * 2));
  } else {
    event.value = formatScore(Mod + Proficiency);
  }
} else {
  if (JackOfAllTrades == 'Yes' && Class == 'Bard') {
    event.value = formatScore(Mod + Math.floor(Proficiency / 2));
  } else if (RemarkableAthlete == 'Yes' && Class == 'Fighter') {
    event.value = formatScore(Mod + Math.ceil(Proficiency / 2));
  } else {
    event.value = formatScore(Mod);
  }
}

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>SkillAnimalHandling:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:SkillAnimalHandling:Calculate ***********/
var Mod               = this.getField('AbilityWISMod').value;
var Checkbox          = this.getField('SkillAnimalHandlingCheckbox').value;
var Expertise         = this.getField('SkillAnimalHandlingExpertiseCheckbox');
var Class             = this.getField('Class').value;
var Proficiency       = this.getField('ProficiencyBonus').value;
var JackOfAllTrades   = this.getField('SkillJackOfAllTradesCheckbox').value;
var RemarkableAthlete = this.getField('SkillRemarkableAthleteCheckbox').value;

if (Checkbox == 'Yes' && Class == 'Rogue') {
  Expertise.display = display.noPrint;
} else {
  Expertise.display = display.hidden;
}

if (Checkbox == 'Yes') {
  if (Expertise.value == 'Yes' && Class == 'Rogue') {
    event.value = formatScore(Mod + (Proficiency * 2));
  } else {
    event.value = formatScore(Mod + Proficiency);
  }
} else {
  if (JackOfAllTrades == 'Yes' && Class == 'Bard') {
    event.value = formatScore(Mod + Math.floor(Proficiency / 2));
  } else {
    event.value = formatScore(Mod);
  }
}

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>SkillArcana:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:SkillArcana:Calculate ***********/
var Mod               = this.getField('AbilityINTMod').value;
var Checkbox          = this.getField('SkillArcanaCheckbox').value;
var Expertise         = this.getField('SkillArcanaExpertiseCheckbox');
var Class             = this.getField('Class').value;
var Proficiency       = this.getField('ProficiencyBonus').value;
var JackOfAllTrades   = this.getField('SkillJackOfAllTradesCheckbox').value;
var RemarkableAthlete = this.getField('SkillRemarkableAthleteCheckbox').value;

if (Checkbox == 'Yes' && Class == 'Rogue') {
  Expertise.display = display.noPrint;
} else {
  Expertise.display = display.hidden;
}

if (Checkbox == 'Yes') {
  if (Expertise.value == 'Yes' && Class == 'Rogue') {
    event.value = formatScore(Mod + (Proficiency * 2));
  } else {
    event.value = formatScore(Mod + Proficiency);
  }
} else {
  if (JackOfAllTrades == 'Yes' && Class == 'Bard') {
    event.value = formatScore(Mod + Math.floor(Proficiency / 2));
  } else {
    event.value = formatScore(Mod);
  }
}

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>SkillAthletics:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:SkillAthletics:Calculate ***********/
var Mod               = this.getField('AbilitySTRMod').value;
var Checkbox          = this.getField('SkillAthleticsCheckbox').value;
var Expertise         = this.getField('SkillAthleticsExpertiseCheckbox');
var Class             = this.getField('Class').value;
var Proficiency       = this.getField('ProficiencyBonus').value;
var JackOfAllTrades   = this.getField('SkillJackOfAllTradesCheckbox').value;
var RemarkableAthlete = this.getField('SkillRemarkableAthleteCheckbox').value;

if (Checkbox == 'Yes' && Class == 'Rogue') {
  Expertise.display = display.noPrint;
} else {
  Expertise.display = display.hidden;
}

if (Checkbox == 'Yes') {
  if (Expertise.value == 'Yes' && Class == 'Rogue') {
    event.value = formatScore(Mod + (Proficiency * 2));
  } else {
    event.value = formatScore(Mod + Proficiency);
  }
} else {
  if (JackOfAllTrades == 'Yes' && Class == 'Bard') {
    event.value = formatScore(Mod + Math.floor(Proficiency / 2));
  } else if (RemarkableAthlete == 'Yes' && Class == 'Fighter') {
    event.value = formatScore(Mod + Math.ceil(Proficiency / 2));
  } else {
    event.value = formatScore(Mod);
  }
}

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>SkillDeception:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:SkillDeception:Calculate ***********/
var Mod               = this.getField('AbilityCHAMod').value;
var Checkbox          = this.getField('SkillDeceptionCheckbox').value;
var Expertise         = this.getField('SkillDeceptionExpertiseCheckbox');
var Class             = this.getField('Class').value;
var Proficiency       = this.getField('ProficiencyBonus').value;
var JackOfAllTrades   = this.getField('SkillJackOfAllTradesCheckbox').value;
var RemarkableAthlete = this.getField('SkillRemarkableAthleteCheckbox').value;

if (Checkbox == 'Yes' && Class == 'Rogue') {
  Expertise.display = display.noPrint;
} else {
  Expertise.display = display.hidden;
}

if (Checkbox == 'Yes') {
  if (Expertise.value == 'Yes' && Class == 'Rogue') {
    event.value = formatScore(Mod + (Proficiency * 2));
  } else {
    event.value = formatScore(Mod + Proficiency);
  }
} else {
  if (JackOfAllTrades == 'Yes' && Class == 'Bard') {
    event.value = formatScore(Mod + Math.floor(Proficiency / 2));
  } else {
    event.value = formatScore(Mod);
  }
}

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>SkillHistory:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:SkillHistory:Calculate ***********/
var Mod               = this.getField('AbilityINTMod').value;
var Checkbox          = this.getField('SkillHistoryCheckbox').value;
var Expertise         = this.getField('SkillHistoryExpertiseCheckbox');
var Class             = this.getField('Class').value;
var Proficiency       = this.getField('ProficiencyBonus').value;
var JackOfAllTrades   = this.getField('SkillJackOfAllTradesCheckbox').value;
var RemarkableAthlete = this.getField('SkillRemarkableAthleteCheckbox').value;

if (Checkbox == 'Yes' && Class == 'Rogue') {
  Expertise.display = display.noPrint;
} else {
  Expertise.display = display.hidden;
}

if (Checkbox == 'Yes') {
  if (Expertise.value == 'Yes' && Class == 'Rogue') {
    event.value = formatScore(Mod + (Proficiency * 2));
  } else {
    event.value = formatScore(Mod + Proficiency);
  }
} else {
  if (JackOfAllTrades == 'Yes' && Class == 'Bard') {
    event.value = formatScore(Mod + Math.floor(Proficiency / 2));
  } else {
    event.value = formatScore(Mod);
  }
}

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>SkillInsight:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:SkillInsight:Calculate ***********/
var Mod               = this.getField('AbilityWISMod').value;
var Checkbox          = this.getField('SkillInsightCheckbox').value;
var Expertise         = this.getField('SkillInsightExpertiseCheckbox');
var Class             = this.getField('Class').value;
var Proficiency       = this.getField('ProficiencyBonus').value;
var JackOfAllTrades   = this.getField('SkillJackOfAllTradesCheckbox').value;
var RemarkableAthlete = this.getField('SkillRemarkableAthleteCheckbox').value;

if (Checkbox == 'Yes' && Class == 'Rogue') {
  Expertise.display = display.noPrint;
} else {
  Expertise.display = display.hidden;
}

if (Checkbox == 'Yes') {
  if (Expertise.value == 'Yes' && Class == 'Rogue') {
    event.value = formatScore(Mod + (Proficiency * 2));
  } else {
    event.value = formatScore(Mod + Proficiency);
  }
} else {
  if (JackOfAllTrades == 'Yes' && Class == 'Bard') {
    event.value = formatScore(Mod + Math.floor(Proficiency / 2));
  } else {
    event.value = formatScore(Mod);
  }
}

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>SkillIntimidation:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:SkillIntimidation:Calculate ***********/
var Mod               = this.getField('AbilityCHAMod').value;
var Checkbox          = this.getField('SkillIntimidationCheckbox').value;
var Expertise         = this.getField('SkillIntimidationExpertiseCheckbox');
var Class             = this.getField('Class').value;
var Proficiency       = this.getField('ProficiencyBonus').value;
var JackOfAllTrades   = this.getField('SkillJackOfAllTradesCheckbox').value;
var RemarkableAthlete = this.getField('SkillRemarkableAthleteCheckbox').value;

if (Checkbox == 'Yes' && Class == 'Rogue') {
  Expertise.display = display.noPrint;
} else {
  Expertise.display = display.hidden;
}

if (Checkbox == 'Yes') {
  if (Expertise.value == 'Yes' && Class == 'Rogue') {
    event.value = formatScore(Mod + (Proficiency * 2));
  } else {
    event.value = formatScore(Mod + Proficiency);
  }
} else {
  if (JackOfAllTrades == 'Yes' && Class == 'Bard') {
    event.value = formatScore(Mod + Math.floor(Proficiency / 2));
  } else {
    event.value = formatScore(Mod);
  }
}

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>SkillInvestigation:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:SkillInvestigation:Calculate ***********/
var Mod               = this.getField('AbilityINTMod').value;
var Checkbox          = this.getField('SkillInvestigationCheckbox').value;
var Expertise         = this.getField('SkillInvestigationExpertiseCheckbox');
var Class             = this.getField('Class').value;
var Proficiency       = this.getField('ProficiencyBonus').value;
var JackOfAllTrades   = this.getField('SkillJackOfAllTradesCheckbox').value;
var RemarkableAthlete = this.getField('SkillRemarkableAthleteCheckbox').value;

if (Checkbox == 'Yes' && Class == 'Rogue') {
  Expertise.display = display.noPrint;
} else {
  Expertise.display = display.hidden;
}

if (Checkbox == 'Yes') {
  if (Expertise.value == 'Yes' && Class == 'Rogue') {
    event.value = formatScore(Mod + (Proficiency * 2));
  } else {
    event.value = formatScore(Mod + Proficiency);
  }
} else {
  if (JackOfAllTrades == 'Yes' && Class == 'Bard') {
    event.value = formatScore(Mod + Math.floor(Proficiency / 2));
  } else {
    event.value = formatScore(Mod);
  }
}

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>SkillJackOfAllTradesText:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:SkillJackOfAllTradesText:Calculate ***********/
var Class    = this.getField('Class').value;
var Text     = this.getField('SkillJackOfAllTradesText');
var Checkbox = this.getField('SkillJackOfAllTradesCheckbox');

if (Class == 'Bard') {
  Text.display     = display.visible;
  Checkbox.display = display.visible;
} else {
  Text.display     = display.hidden;
  Checkbox.display = display.hidden;
}
//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>SkillMedicine:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:SkillMedicine:Calculate ***********/
var Mod               = this.getField('AbilityWISMod').value;
var Checkbox          = this.getField('SkillMedicineCheckbox').value;
var Expertise         = this.getField('SkillMedicineExpertiseCheckbox');
var Class             = this.getField('Class').value;
var Proficiency       = this.getField('ProficiencyBonus').value;
var JackOfAllTrades   = this.getField('SkillJackOfAllTradesCheckbox').value;
var RemarkableAthlete = this.getField('SkillRemarkableAthleteCheckbox').value;

if (Checkbox == 'Yes' && Class == 'Rogue') {
  Expertise.display = display.noPrint;
} else {
  Expertise.display = display.hidden;
}

if (Checkbox == 'Yes') {
  if (Expertise.value == 'Yes' && Class == 'Rogue') {
    event.value = formatScore(Mod + (Proficiency * 2));
  } else {
    event.value = formatScore(Mod + Proficiency);
  }
} else {
  if (JackOfAllTrades == 'Yes' && Class == 'Bard') {
    event.value = formatScore(Mod + Math.floor(Proficiency / 2));
  } else {
    event.value = formatScore(Mod);
  }
}

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>SkillNature:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:SkillNature:Calculate ***********/
var Mod               = this.getField('AbilityINTMod').value;
var Checkbox          = this.getField('SkillNatureCheckbox').value;
var Expertise         = this.getField('SkillNatureExpertiseCheckbox');
var Class             = this.getField('Class').value;
var Proficiency       = this.getField('ProficiencyBonus').value;
var JackOfAllTrades   = this.getField('SkillJackOfAllTradesCheckbox').value;
var RemarkableAthlete = this.getField('SkillRemarkableAthleteCheckbox').value;

if (Checkbox == 'Yes' && Class == 'Rogue') {
  Expertise.display = display.noPrint;
} else {
  Expertise.display = display.hidden;
}

if (Checkbox == 'Yes') {
  if (Expertise.value == 'Yes' && Class == 'Rogue') {
    event.value = formatScore(Mod + (Proficiency * 2));
  } else {
    event.value = formatScore(Mod + Proficiency);
  }
} else {
  if (JackOfAllTrades == 'Yes' && Class == 'Bard') {
    event.value = formatScore(Mod + Math.floor(Proficiency / 2));
  } else {
    event.value = formatScore(Mod);
  }
}

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>SkillPerception:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:SkillPerception:Calculate ***********/
var Mod               = this.getField('AbilityWISMod').value;
var Checkbox          = this.getField('SkillPerceptionCheckbox').value;
var Expertise         = this.getField('SkillPerceptionExpertiseCheckbox');
var Class             = this.getField('Class').value;
var Proficiency       = this.getField('ProficiencyBonus').value;
var JackOfAllTrades   = this.getField('SkillJackOfAllTradesCheckbox').value;
var RemarkableAthlete = this.getField('SkillRemarkableAthleteCheckbox').value;

if (Checkbox == 'Yes' && Class == 'Rogue') {
  Expertise.display = display.noPrint;
} else {
  Expertise.display = display.hidden;
}

if (Checkbox == 'Yes') {
  if (Expertise.value == 'Yes' && Class == 'Rogue') {
    event.value = formatScore(Mod + (Proficiency * 2));
  } else {
    event.value = formatScore(Mod + Proficiency);
  }
} else {
  if (JackOfAllTrades == 'Yes' && Class == 'Bard') {
    event.value = formatScore(Mod + Math.floor(Proficiency / 2));
  } else {
    event.value = formatScore(Mod);
  }
}

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>SkillPerformance:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:SkillPerformance:Calculate ***********/
var Mod               = this.getField('AbilityCHAMod').value;
var Checkbox          = this.getField('SkillPerformanceCheckbox').value;
var Expertise         = this.getField('SkillPerformanceExpertiseCheckbox');
var Class             = this.getField('Class').value;
var Proficiency       = this.getField('ProficiencyBonus').value;
var JackOfAllTrades   = this.getField('SkillJackOfAllTradesCheckbox').value;
var RemarkableAthlete = this.getField('SkillRemarkableAthleteCheckbox').value;

if (Checkbox == 'Yes' && Class == 'Rogue') {
  Expertise.display = display.noPrint;
} else {
  Expertise.display = display.hidden;
}

if (Checkbox == 'Yes') {
  if (Expertise.value == 'Yes' && Class == 'Rogue') {
    event.value = formatScore(Mod + (Proficiency * 2));
  } else {
    event.value = formatScore(Mod + Proficiency);
  }
} else {
  if (JackOfAllTrades == 'Yes' && Class == 'Bard') {
    event.value = formatScore(Mod + Math.floor(Proficiency / 2));
  } else {
    event.value = formatScore(Mod);
  }
}

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>SkillPersuasion:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:SkillPersuasion:Calculate ***********/
var Mod               = this.getField('AbilityCHAMod').value;
var Checkbox          = this.getField('SkillPersuasionCheckbox').value;
var Expertise         = this.getField('SkillPersuasionExpertiseCheckbox');
var Class             = this.getField('Class').value;
var Proficiency       = this.getField('ProficiencyBonus').value;
var JackOfAllTrades   = this.getField('SkillJackOfAllTradesCheckbox').value;
var RemarkableAthlete = this.getField('SkillRemarkableAthleteCheckbox').value;

if (Checkbox == 'Yes' && Class == 'Rogue') {
  Expertise.display = display.noPrint;
} else {
  Expertise.display = display.hidden;
}

if (Checkbox == 'Yes') {
  if (Expertise.value == 'Yes' && Class == 'Rogue') {
    event.value = formatScore(Mod + (Proficiency * 2));
  } else {
    event.value = formatScore(Mod + Proficiency);
  }
} else {
  if (JackOfAllTrades == 'Yes' && Class == 'Bard') {
    event.value = formatScore(Mod + Math.floor(Proficiency / 2));
  } else {
    event.value = formatScore(Mod);
  }
}

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>SkillReligion:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:SkillReligion:Calculate ***********/
var Mod               = this.getField('AbilityINTMod').value;
var Checkbox          = this.getField('SkillReligionCheckbox').value;
var Expertise         = this.getField('SkillReligionExpertiseCheckbox');
var Class             = this.getField('Class').value;
var Proficiency       = this.getField('ProficiencyBonus').value;
var JackOfAllTrades   = this.getField('SkillJackOfAllTradesCheckbox').value;
var RemarkableAthlete = this.getField('SkillRemarkableAthleteCheckbox').value;

if (Checkbox == 'Yes' && Class == 'Rogue') {
  Expertise.display = display.noPrint;
} else {
  Expertise.display = display.hidden;
}

if (Checkbox == 'Yes') {
  if (Expertise.value == 'Yes' && Class == 'Rogue') {
    event.value = formatScore(Mod + (Proficiency * 2));
  } else {
    event.value = formatScore(Mod + Proficiency);
  }
} else {
  if (JackOfAllTrades == 'Yes' && Class == 'Bard') {
    event.value = formatScore(Mod + Math.floor(Proficiency / 2));
  } else {
    event.value = formatScore(Mod);
  }
}

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>SkillRemarkableAthleteText:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:SkillRemarkableAthleteText:Calculate ***********/
var Class    = this.getField('Class').value;
var Text     = this.getField('SkillRemarkableAthleteText');
var Checkbox = this.getField('SkillRemarkableAthleteCheckbox');

if (Class == 'Fighter') {
  Text.display     = display.visible;
  Checkbox.display = display.visible;
} else {
  Text.display     = display.hidden;
  Checkbox.display = display.hidden;
}
//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>SkillSleightOfHand:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:SkillSleightOfHand:Calculate ***********/
var Mod               = this.getField('AbilityDEXMod').value;
var Checkbox          = this.getField('SkillSleightOfHandCheckbox').value;
var Expertise         = this.getField('SkillSleightOfHandExpertiseCheckbox');
var Class             = this.getField('Class').value;
var Proficiency       = this.getField('ProficiencyBonus').value;
var JackOfAllTrades   = this.getField('SkillJackOfAllTradesCheckbox').value;
var RemarkableAthlete = this.getField('SkillRemarkableAthleteCheckbox').value;

if (Checkbox == 'Yes' && Class == 'Rogue') {
  Expertise.display = display.noPrint;
} else {
  Expertise.display = display.hidden;
}

if (Checkbox == 'Yes') {
  if (Expertise.value == 'Yes' && Class == 'Rogue') {
    event.value = formatScore(Mod + (Proficiency * 2));
  } else {
    event.value = formatScore(Mod + Proficiency);
  }
} else {
  if (JackOfAllTrades == 'Yes' && Class == 'Bard') {
    event.value = formatScore(Mod + Math.floor(Proficiency / 2));
  } else if (RemarkableAthlete == 'Yes' && Class == 'Fighter') {
    event.value = formatScore(Mod + Math.ceil(Proficiency / 2));
  } else {
    event.value = formatScore(Mod);
  }
}

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>SkillStealth:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:SkillStealth:Calculate ***********/
var Mod               = this.getField('AbilityDEXMod').value;
var Checkbox          = this.getField('SkillStealthCheckbox').value;
var Expertise         = this.getField('SkillStealthExpertiseCheckbox');
var Class             = this.getField('Class').value;
var Proficiency       = this.getField('ProficiencyBonus').value;
var JackOfAllTrades   = this.getField('SkillJackOfAllTradesCheckbox').value;
var RemarkableAthlete = this.getField('SkillRemarkableAthleteCheckbox').value;

if (Checkbox == 'Yes' && Class == 'Rogue') {
  Expertise.display = display.noPrint;
} else {
  Expertise.display = display.hidden;
}

if (Checkbox == 'Yes') {
  if (Expertise.value == 'Yes' && Class == 'Rogue') {
    event.value = formatScore(Mod + (Proficiency * 2));
  } else {
    event.value = formatScore(Mod + Proficiency);
  }
} else {
  if (JackOfAllTrades == 'Yes' && Class == 'Bard') {
    event.value = formatScore(Mod + Math.floor(Proficiency / 2));
  } else if (RemarkableAthlete == 'Yes' && Class == 'Fighter') {
    event.value = formatScore(Mod + Math.ceil(Proficiency / 2));
  } else {
    event.value = formatScore(Mod);
  }
}

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>SkillSurvival:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:SkillSurvival:Calculate ***********/
var Mod               = this.getField('AbilityWISMod').value;
var Checkbox          = this.getField('SkillSurvivalCheckbox').value;
var Expertise         = this.getField('SkillSurvivalExpertiseCheckbox');
var Class             = this.getField('Class').value;
var Proficiency       = this.getField('ProficiencyBonus').value;
var JackOfAllTrades   = this.getField('SkillJackOfAllTradesCheckbox').value;
var RemarkableAthlete = this.getField('SkillRemarkableAthleteCheckbox').value;

if (Checkbox == 'Yes' && Class == 'Rogue') {
  Expertise.display = display.noPrint;
} else {
  Expertise.display = display.hidden;
}

if (Checkbox == 'Yes') {
  if (Expertise.value == 'Yes' && Class == 'Rogue') {
    event.value = formatScore(Mod + (Proficiency * 2));
  } else {
    event.value = formatScore(Mod + Proficiency);
  }
} else {
  if (JackOfAllTrades == 'Yes' && Class == 'Bard') {
    event.value = formatScore(Mod + Math.floor(Proficiency / 2));
  } else {
    event.value = formatScore(Mod);
  }
}

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>SpellAttackBonus:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:SpellAttackBonus:Calculate ***********/
var Class            = this.getField('Class').value;
var Proficiency      = this.getField('ProficiencyBonus').value;
var INTMod           = this.getField('AbilityINTMod').value;
var WISMod           = this.getField('AbilityWISMod').value;
var CHAMod           = this.getField('AbilityCHAMod').value;
var SpellAttackBonus = 0;

switch (Class) {
  case 'Bard':
  case 'Paladin':
  case 'Sorcerer':
  case 'Warlock':
    SpellAttackBonus = Proficiency + CHAMod;
    break;
  case 'Cleric':
  case 'Druid':
  case 'Ranger':
    SpellAttackBonus = Proficiency + WISMod;
    break;
  case 'Wizard':
    SpellAttackBonus = Proficiency + INTMod;
    break;
  case 'Barbarian':
  case 'Fighter':
  case 'Monk':
  case 'Rogue':
    SpellAttackBonus = '';
    break;
  default:
    break;
}

event.value = formatScore(SpellAttackBonus);
//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>SpellSaveDC:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:SpellSaveDC:Calculate ***********/
var Class       = this.getField('Class').value;
var Proficiency = this.getField('ProficiencyBonus').value;
var INTMod      = this.getField('AbilityINTMod').value;
var WISMod      = this.getField('AbilityWISMod').value;
var CHAMod      = this.getField('AbilityCHAMod').value;
var SpellSaveDC = 8 + Proficiency;

switch (Class) {
  case 'Bard':
  case 'Paladin':
  case 'Sorcerer':
  case 'Warlock':
    SpellSaveDC = SpellSaveDC + CHAMod;
    break;
  case 'Cleric':
  case 'Druid':
  case 'Ranger':
    SpellSaveDC = SpellSaveDC + WISMod;
    break;
  case 'Wizard':
    SpellSaveDC = SpellSaveDC + INTMod;
    break;
  case 'Barbarian':
  case 'Fighter':
  case 'Monk':
  case 'Rogue':
    SpellSaveDC = '';
    break;
  default:
    break;
}

event.value = SpellSaveDC;
//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>SpellcastingAbility:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:SpellcastingAbility:Calculate ***********/
var Class               = this.getField('Class').value;
var SpellcastingAbility = '';

switch (Class) {
  case 'Bard':
  case 'Paladin':
  case 'Sorcerer':
  case 'Warlock':
    SpellcastingAbility = 'Charisma';
    break;
  case 'Cleric':
  case 'Druid':
  case 'Ranger':
    SpellcastingAbility = 'Wisdom';
    break;
  case 'Wizard':
    SpellcastingAbility = 'Intelligence';
    break;
  case 'Barbarian':
  case 'Fighter':
  case 'Monk':
  case 'Rogue':
    break;
  default:
    break;
}

event.value = SpellcastingAbility;
//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>SpellcastingClass:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:SpellcastingClass:Calculate ***********/
event.value = this.getField("Class").value;
//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>SpellsSpellAttackBonus:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:SpellsSpellAttackBonus:Calculate ***********/
event.value = formatScore(this.getField('SpellAttackBonus').value);
//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>SpellsSpellSaveDC:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:SpellsSpellSaveDC:Calculate ***********/
event.value = this.getField('SpellSaveDC').value;
//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>SpellsSpellcastingAbility:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:SpellsSpellcastingAbility:Calculate ***********/
event.value = this.getField('SpellcastingAbility').value;
//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>VariantHumanTraitText:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:VariantHumanTraitText:Calculate ***********/
var Race     = this.getField('Race').value;
var Text     = this.getField('VariantHumanTraitText');
var Checkbox = this.getField('VariantHumanTraitCheckbox');

if (Race == 'Human') {
  Text.display     = display.visible;
  Checkbox.display = display.visible;
} else {
  Text.display     = display.hidden;
  Checkbox.display = display.hidden;
}
//</ACRO_script>
//</AcroForm>
