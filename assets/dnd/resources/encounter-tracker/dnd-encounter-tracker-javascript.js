//-------------------------------------------------------------
//-----------------Do not edit the XML tags--------------------
//-------------------------------------------------------------

//<Document-Level>
//<ACRO_source>challengeRatingXP</ACRO_source>
//<ACRO_script>
/*********** belongs to: Document-Level:challengeRatingXP ***********/
function challengeRatingXP (cr, zeroHasAttacks) {
  // Used if monsters with 0 CR give has attacks or not. If they do they award 10 XP, otherwise 0 XP.
  var zeroHasAttacks = typeof zeroHasAttacks !== 'undefined' ? zeroHasAttacks : false;
  var xp = {
    '0': 0,
    '1/8': 25,
    '1/4': 50,
    '1/2': 100,
    '1': 200,
    '2': 450,
    '3': 700,
    '4': 1100,
    '5': 1800,
    '6': 2300,
    '7': 2900,
    '8': 3900,
    '9': 5000,
    '10': 5900,
    '11': 7200,
    '12': 8400,
    '13': 10000,
    '14': 11500,
    '15': 13000,
    '16': 15000,
    '17': 18000,
    '18': 20000,
    '19': 22000,
    '20': 25000,
    '21': 33000,
    '22': 41000,
    '23': 50000,
    '24': 62000,
    '25': 75000,
    '26': 90000,
    '27': 105000,
    '28': 120000,
    '29': 135000,
    '30': 155000
  };

  if (cr < 0 || cr > 30 || cr == '') {
    return 0;
  } else if (cr == 0 && zeroHasAttacks) {
    return 10;
  } else {
    return xp[cr];
  }
}

//</ACRO_script>
//</Document-Level>

//<Document-Level>
//<ACRO_source>formatScore</ACRO_source>
//<ACRO_script>
/*********** belongs to: Document-Level:formatScore ***********/
function formatScore (score) {
  if (score > 0) {
    return "+" + score;
  } else {
    return score;
  }
}
//</ACRO_script>
//</Document-Level>

//<AcroForm>
//<ACRO_source>Monster1CHAMod:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:Monster1CHAMod:Calculate ***********/
var score = formatScore(Math.floor((this.getField("Monster1CHA").value - 10) / 2));

event.value = score;

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>Monster1CONMod:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:Monster1CONMod:Calculate ***********/
var score = formatScore(Math.floor((this.getField("Monster1CON").value - 10) / 2));

event.value = score;

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>Monster1DEXMod:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:Monster1DEXMod:Calculate ***********/
var score = formatScore(Math.floor((this.getField("Monster1DEX").value - 10) / 2));

event.value = score;

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>Monster1INTMod:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:Monster1INTMod:Calculate ***********/
var score = formatScore(Math.floor((this.getField("Monster1INT").value - 10) / 2));

event.value = score;

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>Monster1Initiative:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:Monster1Initiative:Calculate ***********/
event.value = formatScore(this.getField('Monster1DEXMod').value);
//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>Monster1STRMod:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:Monster1STRMod:Calculate ***********/
var score = formatScore(Math.floor((this.getField("Monster1STR").value - 10) / 2));

event.value = score;

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>Monster1WISMod:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:Monster1WISMod:Calculate ***********/
var score = formatScore(Math.floor((this.getField("Monster1WIS").value - 10) / 2));

event.value = score;

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>Monster1XP:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:Monster1XP:Calculate ***********/
var cr = this.getField('Monster1Challenge').value;
var xp = challengeRatingXP(cr);

event.value = xp;

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>Monster2CHAMod:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:Monster2CHAMod:Calculate ***********/
var score = formatScore(Math.floor((this.getField("Monster2CHA").value - 10) / 2));

event.value = score;

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>Monster2CONMod:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:Monster2CONMod:Calculate ***********/
var score = formatScore(Math.floor((this.getField("Monster2CON").value - 10) / 2));

event.value = score;

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>Monster2DEXMod:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:Monster2DEXMod:Calculate ***********/
var score = formatScore(Math.floor((this.getField("Monster2DEX").value - 10) / 2));

event.value = score;

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>Monster2INTMod:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:Monster2INTMod:Calculate ***********/
var score = formatScore(Math.floor((this.getField("Monster2INT").value - 10) / 2));

event.value = score;

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>Monster2Initiative:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:Monster2Initiative:Calculate ***********/
event.value = formatScore(this.getField('Monster2DEXMod').value);
//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>Monster2STRMod:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:Monster2STRMod:Calculate ***********/
var score = formatScore(Math.floor((this.getField("Monster2STR").value - 10) / 2));

event.value = score;

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>Monster2WISMod:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:Monster2WISMod:Calculate ***********/
var score = formatScore(Math.floor((this.getField("Monster2WIS").value - 10) / 2));

event.value = score;

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>Monster2XP:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:Monster2XP:Calculate ***********/
var cr = this.getField('Monster2Challenge').value;
var xp = challengeRatingXP(cr);

event.value = xp;

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>Monster3CHAMod:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:Monster3CHAMod:Calculate ***********/
var score = formatScore(Math.floor((this.getField("Monster3CHA").value - 10) / 2));

event.value = score;

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>Monster3CONMod:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:Monster3CONMod:Calculate ***********/
var score = formatScore(Math.floor((this.getField("Monster3CON").value - 10) / 2));

event.value = score;

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>Monster3DEXMod:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:Monster3DEXMod:Calculate ***********/
var score = formatScore(Math.floor((this.getField("Monster3DEX").value - 10) / 2));

event.value = score;

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>Monster3INTMod:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:Monster3INTMod:Calculate ***********/
var score = formatScore(Math.floor((this.getField("Monster3INT").value - 10) / 2));

event.value = score;

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>Monster3Initiative:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:Monster3Initiative:Calculate ***********/
event.value = formatScore(this.getField('Monster3DEXMod').value);
//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>Monster3STRMod:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:Monster3STRMod:Calculate ***********/
var score = formatScore(Math.floor((this.getField("Monster3STR").value - 10) / 2));

event.value = score;

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>Monster3WISMod:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:Monster3WISMod:Calculate ***********/
var score = formatScore(Math.floor((this.getField("Monster3WIS").value - 10) / 2));

event.value = score;

//</ACRO_script>
//</AcroForm>

//<AcroForm>
//<ACRO_source>Monster3XP:Calculate</ACRO_source>
//<ACRO_script>
/*********** belongs to: AcroForm:Monster3XP:Calculate ***********/
var cr = this.getField('Monster3Challenge').value;
var xp = challengeRatingXP(cr);

event.value = xp;

//</ACRO_script>
//</AcroForm>
