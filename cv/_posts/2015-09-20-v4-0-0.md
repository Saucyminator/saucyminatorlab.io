---
layout:           post
title:            CV v4.0.0
date:             2015-09-20 17:28
version:          4.0.0
assets_separated: false # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
buttons:
  downloads:      # array of files inside /assets folder or external links
    - name:       v4.0.0
      url:        cv-v4.0.0.pdf
---

# Patrik Holmberg
Självlärd inom webbutveckling då jag varit intresserad av programmering och själv skapat egna projekt i ca 10 år. Primärt lärt mig PHP tillsammans med MySQL, men på senaste år fokuserat mer på JavaScript.

Jag är organiserad, lugn och noggrann person som älskar lära mig nya saker. Väldigt passionerad med det jag gör, mycket nyfiken på hur saker fungerar för se om det går att förbättra det.

På min fritid försöker jag ständigt förbättra mina kunskaper inom programmering. Mina mål inom webbutveckling är att utvidga mina kunskaper och lära mig om de nya teknikerna som ständigt utvecklas.

## Webbutveckling
- PHP
- JavaScript, AngularJS
- HTML5, CSS3, LESS
- Git; GitHub, Bitbucket
- Bower, Gulp, Node.js (npm)

## Databas & webbserver
- MySQL, phpMyAdmin
- Apache (WAMP/XAMP)

## Webbsidor jag skapat
- [https://saucy.se](https://saucy.se)

## Design
- Adobe Photoshop

## Allmänna kunskaper
- Google Drive, MS Office & OpenOffice
- Operativsystem: Windows, GNU/Linux

## Yrkeserfarenhet
- Höörs Kommun
  - Praktikant, Höör - 2015
- Facetime Media AB
  - Provanställning/Praktikant (Programmerare), Lund - 2013
- Ica Supermarket
  - Praktikant (Lager & varuplockning), Höör - 2004
- Bilema
  - Praktikant (Lager & varuplockning), Höör - 2003

## Utbildning
- Komvux
  - Läst upp gymnasiebetyg, Höör - 2009-2011
- Salliusgymnasiet
  - Handelsprogrammet, Eslöv - 2004-2007

## Språkkunskaper
- Svenska
  - Modersmål.
- Engelska
  - Flytande i tal och skrift.

## Referenser
- Referenser och intyg kan fås på begäran.

---

- **Namn**: Patrik Holmberg
- **Personnummer**: 1988-03-31
- **Adress**: Nya Torg 10, lgh 1105 - 24330 Höör
- **Körkort**: - Inget -
- **Telefon**: 0730-33 88 76
- **Email**: work@saucy.se
- **Hemsida**: [https://saucy.se](https://saucy.se)
- **Github**: [https://github.com/Saucy](https://github.com/Saucy)
