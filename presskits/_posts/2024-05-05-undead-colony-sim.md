---
published:        false
layout:           post-presskit
title:            Undead Colony Sim (WIP)
date:             2024-05-05 17:17
date_updated:     2024-06-30 16:56
tags:             []
summary:
assets_separated: true # If assets are separated by posts or collected at the root of the respective folder. Example: true - /assets/folder/POST-ID/<asset> vs false - /assets/folder/<asset>
is_studio:        false # true = game
game_specifics:
  studio_presskit_url: https://saucy.se/presskits/studio/
  release_date: 2025-01-01 00:01
  platforms:
    # - Steam (Windows)
  regular_prices:
  features:
  steam_widget:
  about_the_developer: <p>Silent Creativity consists of two friends who like programming, games, art creation, and music creation.</p>
studio_specifics:
  founding_date:
  business_contact_email:
  social:
  releases:
  address:
  phone:
factsheet:
  developer:
    name: Silent Creativity
    url: https://saucy.se
    location: Sweden
  website:
    name: saucy.se
    url: https://saucy.se
description: <p>TODO</p>
history: <h3>TODO</h3><p>TODO</p>
videos:
images:
  banner: banner.png
  thumbnail: banner_thumbnail.png
  screenshots:
logos:
additional_links:
credits:
  - name: Patrik Holmberg
    work_title: Founder, Programmer, The gamedev
    url: # https://saucy.se
contact:
  - title: Inquiries
    url: support@saucy.se
  - title: Web
    url: https://saucy.se
buttons: # files located in the /assets folder or external links
  request_press_copy: # TODO: What to have here?
  download_images:
  download_logos:
---
