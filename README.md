# saucy.se
Source code for my personal website at [Saucy.se][].


## Who am I?
My name is Patrik Holmberg, born in '88. I usually go by the alias _Saucy_, however I use a few variations of that if it is already taken.

I code for fun! My previous personal websites were disorganized and we didn't have [Git][] back then. However these days I'm a bit more organized and I strive to keep things clean!


## Why I am doing this?
I love to learn new things. However I rarely read any books so I usually end up doing things by trial and error.

Before [Git][] I had lots of code everywhere on my computer (endless backups). Mostly code that I quickly put together just because I wanted see how/if it worked. I have my old legacy code on here (but more up to date and organized)!


## Education
I am completely self-taught, everything I know I've learned from tutorials on the Internet.


## What am I up to these days?
I mostly gave up on webdev and am now focusing on becoming a better Unity3D developer. As you can see on my [saucy.se/games][] page I keep doing prototypes lol.


## Development (locally using Docker)
1. Fork, clone or download this project  
2. Download and setup Docker Desktop from [docker.com][]  
3. Download and setup VS Code from [code.visualstudio.com][]  
  a. Install both the "Docker" and "Dev Containers" plugins  
4. Open the folder with VSCode and a popup should be shown of folder containing a Dev Container configuration file. Choose "**Reopen in Container**"  
  a. ![](src/images/docs/vscode_dev_container_popup.png)  
5. Run the following commands:  
  a. ```gem update --system```  
  a. ```bundle install```  
6. Command to access the website: ```bundle exec jekyll serve --force_polling --livereload```  
  a. Open website locally at http://127.0.0.1:4000/  

Read more at Jekyll's [documentation][] and more information is available at GitLab's [Jekyll pages][].


## License
[MIT License][]


[saucy.se]: https://saucy.se
[git]: https://git-scm.com/
[saucy.se/games]: https://saucy.se/games/
[docker.com]: https://www.docker.com/
[code.visualstudio.com]: https://code.visualstudio.com/
[install]: https://jekyllrb.com/docs/installation/
[documentation]: https://jekyllrb.com/docs/home/
[jekyll pages]: https://gitlab.com/pages/jekyll
[mit license]: LICENSE.md
