---
layout:           post-legacy
title:            Kvarnstensbockarna.se
date:             2010-11-15
date_updated:     2016-04-15 16:57
tags:             [Websites, HTML, CSS, JavaScript, PHP, SQL]
summary:          Website for my father and their club.
images:
  banner:         https://legacy.saucy.se/kvarnstensbockarna.se/assets/images/banner.png # 1920x540
  thumbnail:      https://legacy.saucy.se/kvarnstensbockarna.se/assets/images/thumbnail.png # 384x216
  screenshot:     https://legacy.saucy.se/kvarnstensbockarna.se/assets/images/screenshot.png # 1920x1080
  screenshots:    # thumbnail: 384x216, screenshot: 1920x1080
buttons:
  demo:           # external link
  source:         https://gitlab.com/Saucyminator/legacy.saucy.se/tree/master/kvarnstensbockarna.se # external link or link to original source
information:
  released:       Not long after creation
  client:         My father and their club
  status:         Completed
---

My father asked me to create a new website for their club.

I DARE you to look at the source code, oh dear lord. (As long as it works amirite?)

Update 2017-03-30: My father does not own the kvarnstensbockarna.se domain any longer. Before it went down, I made a jekyll version of their site (and updated overall website a bit), you can find the source code on [GitLab](https://gitlab.com/Saucyminator/legacy.saucy.se/tree/master/kvarnstensbockarna.se).
