---
layout:           post-legacy
title:            Nightmare.Offline
date:             2006-03-19
date_updated:     
tags:             [Websites, HTML, CSS, Flash]
summary:          Another concept/website for our CS 1.6 clan.
images:
  banner:         https://legacy.saucy.se/nightmare.offline/assets/images/banner.png # 1920x540
  thumbnail:      https://legacy.saucy.se/nightmare.offline/assets/images/thumbnail.png # 384x216
  screenshot:     https://legacy.saucy.se/nightmare.offline/assets/images/screenshot.png # 1920x1080
  screenshots:    # thumbnail: 384x216, screenshot: 1920x1080
buttons:
  demo:           https://legacy.saucy.se/nightmare.offline/ # external link
  source:         https://gitlab.com/Saucyminator/legacy.saucy.se/tree/master/nightmare.offline # external link or link to original source
information:
  released:       Not released
  client:         Our CS 1.6 clan
  status:         Never completed
---

The 2:nd website I created for the CS 1.6 clan I were in. We had changed the name from Lame.Losers to Nightmare.Offline.

Not sure why the clan leader thought it was a good idea. After a while the clan went under and I obviously stopped working on the website.
