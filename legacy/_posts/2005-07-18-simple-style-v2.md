---
layout:           post-legacy
title:            Simple-Style v2
date:             2005-07-18
date_updated:     
tags:             [Websites, HTML, CSS, JavaScript]
summary:          Full height, slim and centered website.
images:
  banner:         https://legacy.saucy.se/simple-style-v2/assets/images/banner.png # 1920x540
  thumbnail:      https://legacy.saucy.se/simple-style-v2/assets/images/thumbnail.png # 384x216
  screenshot:     https://legacy.saucy.se/simple-style-v2/assets/images/screenshot.png # 1920x1080
  screenshots:    # thumbnail: 384x216, screenshot: 1920x1080
buttons:
  demo:           https://legacy.saucy.se/simple-style-v2/ # external link
  source:         https://gitlab.com/Saucyminator/legacy.saucy.se/tree/master/simple-style-v2 # external link or link to original source
information:
  released:       Not released
  client:         Myself
  status:         Never completed
---

An inverted colors of Simple-Style version.

Trying out full height instead of cutting off after the content. The website felt more complex than the original version.
