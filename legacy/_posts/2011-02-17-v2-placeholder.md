---
layout:           post-legacy
title:            v2 - Placeholder
date:             2011-02-17
date_updated:     
tags:             [Websites, HTML, CSS, JavaScript, PHP]
summary:          Placeholder website for version 2 of Saucy.se.
images:
  banner:         https://legacy.saucy.se/v2-ph/assets/images/banner.png # 1920x540
  thumbnail:      https://legacy.saucy.se/v2-ph/assets/images/thumbnail.png # 384x216
  screenshot:     https://legacy.saucy.se/v2-ph/assets/images/screenshot.png # 1920x1080
  screenshots:    # thumbnail: 384x216, screenshot: 1920x1080
buttons:
  demo:           https://legacy.saucy.se/v2-ph/ # external link
  source:         https://gitlab.com/Saucyminator/legacy.saucy.se/tree/master/v2-ph # external link or link to original source
information:
  released:       Not sure when released
  client:         Myself
  status:         Never completed
---

Another famous example of my "website is under construction" page.

Here I started on the idea of everything I use on the page should be a row in the database.

Never got to actually implement the database, so I used a big freaking multidimensional array instead (see v2 source code: /inc/settings.php file).
