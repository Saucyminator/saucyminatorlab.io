---
layout:           post-legacy
title:            v4 - Placeholder
date:             2014-01-11
date_updated:     
tags:             [Websites, HTML, CSS, JavaScript, AngularJS, Bootstrap, jQuery, Bower]
summary:          Placeholder website for version 4 of Saucy.se.
images:
  banner:         https://legacy.saucy.se/v4-ph/assets/images/banner.png # 1920x540
  thumbnail:      https://legacy.saucy.se/v4-ph/assets/images/thumbnail.png # 384x216
  screenshot:     https://legacy.saucy.se/v4-ph/assets/images/screenshot.png # 1920x1080
  screenshots:    # thumbnail: 384x216, screenshot: 1920x1080
buttons:
  demo:           https://legacy.saucy.se/v4-ph/ # external link
  source:         https://gitlab.com/Saucyminator/legacy.saucy.se/tree/master/v4-ph # external link or link to original source
information:
  released:       Not sure when released
  client:         Myself
  status:         Never completed
---

The start of using AngularJS via Yeoman (using a AngularJS-generator).

Introduced to Bower and Grunt.
