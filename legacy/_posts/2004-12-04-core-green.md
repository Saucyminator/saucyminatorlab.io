---
layout:           post-legacy
title:            Core Green
date:             2004-12-04
date_updated:     
tags:             [Websites, HTML, CSS]
summary:          Continuing expanding my Photoshop skills.
images:
  banner:         https://legacy.saucy.se/core-green/assets/images/banner.png # 1920x540
  thumbnail:      https://legacy.saucy.se/core-green/assets/images/thumbnail.png # 384x216
  screenshot:     https://legacy.saucy.se/core-green/assets/images/screenshot.png # 1920x1080
  screenshots:    # thumbnail: 384x216, screenshot: 1920x1080
buttons:
  demo:           https://legacy.saucy.se/core-green/ # external link
  source:         https://gitlab.com/Saucyminator/legacy.saucy.se/tree/master/core-green # external link or link to original source
information:
  released:       Not released
  client:         Myself
  status:         Never completed
---

Shortly after I made my first website, I made this one.

I was trying out new styles in Photoshop (just take a look at the banner), slowly building my Photoshop skills!
