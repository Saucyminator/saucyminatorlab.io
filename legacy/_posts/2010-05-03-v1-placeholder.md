---
layout:           post-legacy
title:            v1 - Placeholder
date:             2010-05-03
date_updated:     
tags:             [Websites, HTML, CSS, PHP]
summary:          Placeholder website for version 1 of Saucy.se.
images:
  banner:         https://legacy.saucy.se/v1-ph/assets/images/banner.png # 1920x540
  thumbnail:      https://legacy.saucy.se/v1-ph/assets/images/thumbnail.png # 384x216
  screenshot:     https://legacy.saucy.se/v1-ph/assets/images/screenshot.png # 1920x1080
  screenshots:    # thumbnail: 384x216, screenshot: 1920x1080
buttons:
  demo:           https://legacy.saucy.se/v1-ph/ # external link
  source:         https://gitlab.com/Saucyminator/legacy.saucy.se/tree/master/v1-ph # external link or link to original source
information:
  released:       Not sure when released
  client:         Myself
  status:         Completed
---

The start of Saucy.se! It's only a background image I made in Photoshop.

The "symbol" would represent an "S" and a "C". Somehow it would represent the word/name Saucy.
