---
layout:           post-legacy
title:            Rusty-Steel
date:             2005-10-20
date_updated:     
tags:             [Websites, HTML, CSS]
summary:          Right side-oriented website.
images:
  banner:         https://legacy.saucy.se/rusty-steel/assets/images/banner.png # 1920x540
  thumbnail:      https://legacy.saucy.se/rusty-steel/assets/images/thumbnail.png # 384x216
  screenshot:     https://legacy.saucy.se/rusty-steel/assets/images/screenshot.png # 1920x1080
  screenshots:    # thumbnail: 384x216, screenshot: 1920x1080
buttons:
  demo:           https://legacy.saucy.se/rusty-steel/ # external link
  source:         https://gitlab.com/Saucyminator/legacy.saucy.se/tree/master/rusty-steel # external link or link to original source
information:
  released:       Not released
  client:         Myself
  status:         Never completed
---

Tried a very different approach on this one!

I had seen many websites with their menu at the top or left side of the page. Of course I wanted to be unique and created a site with the menu on the right.

At a later point I realized it was more smoother for the users to have the menu on the left side/top side.

Also first thoughts on how to update the site with PHP (news should have been displayed on the main page etc.).
