---
layout:           post-legacy
title:            v3 - Placeholder
date:             2013-02-06
date_updated:     
tags:             [Websites, HTML, CSS, PHP]
summary:          Placeholder website for version 3 of Saucy.se.
images:
  banner:         https://legacy.saucy.se/v3-ph/assets/images/banner.png # 1920x540
  thumbnail:      https://legacy.saucy.se/v3-ph/assets/images/thumbnail.png # 384x216
  screenshot:     https://legacy.saucy.se/v3-ph/assets/images/screenshot.png # 1920x1080
  screenshots:    # thumbnail: 384x216, screenshot: 1920x1080
buttons:
  demo:           https://legacy.saucy.se/v3-ph/ # external link
  source:         https://gitlab.com/Saucyminator/legacy.saucy.se/tree/master/v3-ph # external link or link to original source
information:
  released:       Not sure when released
  client:         Myself
  status:         Never completed
---

Yet another "under construction page". I think I had more fun doing these than actually building new websites instead, oh well lol.

I got my first job as an application developer, I had to build a blog to get an interview. I nailed it!

Under 5.5 months I learned a lot from the colleagues there. Introduced to Git, Bootstrap, LESS, Slim Framework and AngularJS.

This was the time I went from Procedural code to OOP in PHP. I spent a lot of time planning what features, the file structure should be, etc.

I'm not sure how much impact my colleagues think they had on me, but I guarantee it was a lot. What I learned from my time there have helped me shape into how I code and how to structure things.

I'd like to say thank you, you know who you are.
