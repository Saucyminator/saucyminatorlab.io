---
layout:           post-legacy
title:            Frostapaintball.se
date:             2013-11-20
date_updated:     
tags:             [Websites, HTML, CSS, JavaScript, PHP, SQL]
summary:          For a paintball organization.
images:
  banner:         https://legacy.saucy.se/frostapaintball.se/assets/images/banner.png # 1920x540
  thumbnail:      https://legacy.saucy.se/frostapaintball.se/assets/images/thumbnail.png # 384x216
  screenshot:     https://legacy.saucy.se/frostapaintball.se/assets/images/screenshot.png # 1920x1080
  screenshots:    # thumbnail: 384x216, screenshot: 1920x1080
buttons:
  demo:           https://legacy.saucy.se/frostapaintball.se/ # external link
  source:         https://gitlab.com/Saucyminator/legacy.saucy.se/tree/master/frostapaintball.se # external link or link to original source
information:
  released:       2010-10-13 (no exact date)
  client:         Local Paintball organization (run by friends)
  status:         Completed
---

Quick website I made for my friends and I, who started a Paintball organization back in 2010.

Based on my [v2](https://saucy.se/legacy/v2/) website.