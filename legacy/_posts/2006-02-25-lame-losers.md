---
layout:           post-legacy
title:            Lame.Losers
date:             2006-02-25
date_updated:     
tags:             [Websites, HTML, CSS, JavaScript]
summary:          Concept/website for our CS 1.6 clan.
images:
  banner:         https://legacy.saucy.se/lame.losers/assets/images/banner.png # 1920x540
  thumbnail:      https://legacy.saucy.se/lame.losers/assets/images/thumbnail.png # 384x216
  screenshot:     https://legacy.saucy.se/lame.losers/assets/images/screenshot.png # 1920x1080
  screenshots:    # thumbnail: 384x216, screenshot: 1920x1080
buttons:
  demo:           https://legacy.saucy.se/lame.losers/ # external link
  source:         https://gitlab.com/Saucyminator/legacy.saucy.se/tree/master/lame.losers # external link or link to original source
information:
  released:       Not released
  client:         Our CS 1.6 clan
  status:         Never completed
---

A website I created for my CS 1.6 clan. Got closer learning PHP, but never came that far to use it on the website.

_(People's information have been edited out)_
