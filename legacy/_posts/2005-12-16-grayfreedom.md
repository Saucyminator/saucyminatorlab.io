---
layout:           post-legacy
title:            GrayFreedom
date:             2005-12-16
date_updated:     
tags:             [Websites, HTML, CSS, Flash]
summary:          A gray website with a flash menu.
images:
  banner:         https://legacy.saucy.se/grayfreedom/assets/images/banner.png # 1920x540
  thumbnail:      https://legacy.saucy.se/grayfreedom/assets/images/thumbnail.png # 384x216
  screenshot:     https://legacy.saucy.se/grayfreedom/assets/images/screenshot.png # 1920x1080
  screenshots:    # thumbnail: 384x216, screenshot: 1920x1080
buttons:
  demo:           https://legacy.saucy.se/grayfreedom/ # external link
  source:         https://gitlab.com/Saucyminator/legacy.saucy.se/tree/master/grayfreedom # external link or link to original source
information:
  released:       Not sure when released
  client:         Myself
  status:         Near completed
---

I spent extra time on the design for this website. I had learned how to make basic buttons in Flash at the time, and showing off the cool effects I had learned.

Quite proud of the design and that I had learned basic Flash. I found a small Danish web hosting service (with no ads injection on the pages!).

I made changes to the website weekly but I later got bored (and probably too busy playing WoW!), so I stopped working on it after a few months.
