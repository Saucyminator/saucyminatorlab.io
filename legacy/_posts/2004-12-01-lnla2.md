---
layout:           post-legacy
title:            lnla2
date:             2004-12-01
date_updated:     
tags:             [Websites, HTML, CSS]
summary:          The first graphical website.
images:
  banner:         https://legacy.saucy.se/lnla2/assets/images/banner.png # 1920x540
  thumbnail:      https://legacy.saucy.se/lnla2/assets/images/thumbnail.png # 384x216
  screenshot:     https://legacy.saucy.se/lnla2/assets/images/screenshot.png # 1920x1080
  screenshots:    # thumbnail: 384x216, screenshot: 1920x1080
buttons:
  demo:           https://legacy.saucy.se/lnla2/ # external link
  source:         https://gitlab.com/Saucyminator/legacy.saucy.se/tree/master/lnla2 # external link or link to original source
information:
  released:       Not released
  client:         Myself
  status:         Never completed
---

The very first website I've made, except for a few pages (done in Notepad). This was the first graphical website I made.

_Backstory: I was playing CS 1.6 and randomly typed a few letters, that's how lnla2 came to existence. I liked my newly formed nickname at the time._
