---
layout:           post-legacy
title:            Leonardo Da Vinci
date:             2006-02-02
date_updated:     
tags:             [Websites, HTML, CSS]
summary:          A friends school project about Leonardo Da Vinci.
images:
  banner:         https://legacy.saucy.se/leonardo-da-vinci/assets/images/banner.png # 1920x540
  thumbnail:      https://legacy.saucy.se/leonardo-da-vinci/assets/images/thumbnail.png # 384x216
  screenshot:     https://legacy.saucy.se/leonardo-da-vinci/assets/images/screenshot.png # 1920x1080
  screenshots:    # thumbnail: 384x216, screenshot: 1920x1080
buttons:
  demo:           https://legacy.saucy.se/leonardo-da-vinci/ # external link
  source:         https://gitlab.com/Saucyminator/legacy.saucy.se/tree/master/leonardo-da-vinci # external link or link to original source
information:
  released:       Friend used it in a school project
  client:         Friend
  status:         Completed
---

A quick and clean website I made for a friend. If I remember correctly this was for a school project a friend of mine had.

I offered my help. I later found out she (and the group she was working with at the school) had changed the page.

I remember I got a little upset because they changed it without telling me of what I could improve.
