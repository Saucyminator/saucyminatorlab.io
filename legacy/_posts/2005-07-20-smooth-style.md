---
layout:           post-legacy
title:            Smooth-Style
date:             2005-07-20
date_updated:     
tags:             [Websites, HTML, CSS]
summary:          Smooth colors using gradients.
images:
  banner:         https://legacy.saucy.se/smooth-style/assets/images/banner.png # 1920x540
  thumbnail:      https://legacy.saucy.se/smooth-style/assets/images/thumbnail.png # 384x216
  screenshot:     https://legacy.saucy.se/smooth-style/assets/images/screenshot.png # 1920x1080
  screenshots:    # thumbnail: 384x216, screenshot: 1920x1080
buttons:
  demo:           https://legacy.saucy.se/smooth-style/ # external link
  source:         https://gitlab.com/Saucyminator/legacy.saucy.se/tree/master/smooth-style # external link or link to original source
information:
  released:       Not released
  client:         Myself
  status:         Never completed
---

I thought it sounded professional if I added "-style" to my websites... (it does not).

Anyway, here I tried smooth colors by using gradient and such. It felt pretty and fresh.
