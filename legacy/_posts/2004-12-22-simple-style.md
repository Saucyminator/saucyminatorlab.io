---
layout:           post-legacy
title:            Simple-Style
date:             2004-12-22
date_updated:     
tags:             [Websites, HTML, CSS]
summary:          Minimalistic themed website.
images:
  banner:         https://legacy.saucy.se/simple-style/assets/images/banner.png # 1920x540
  thumbnail:      https://legacy.saucy.se/simple-style/assets/images/thumbnail.png # 384x216
  screenshot:     https://legacy.saucy.se/simple-style/assets/images/screenshot.png # 1920x1080
  screenshots:    # thumbnail: 384x216, screenshot: 1920x1080
buttons:
  demo:           https://legacy.saucy.se/simple-style/ # external link
  source:         https://gitlab.com/Saucyminator/legacy.saucy.se/tree/master/simple-style # external link or link to original source
information:
  released:       Not sure when released
  client:         Myself
  status:         Near completed
---

This was my first website I were proud to show.

I love dark themes and a minimalistic feel of a website. I was still learning Photoshop!
