---
layout:           post
title:            'Unreal Engine 4: TileSets and TileMaps!'
date:             2015-12-10 23:16
date_updated:     2016-05-17 00:52
tags:             [GameDev, Games, Curse of Undeath, Prototype, Unreal Engine 4, TileSet, TileMap, Tutorial]
summary:          Let's talk about TileSets and TileMaps in Unreal Engine 4!
images:
  banner:         banner.png # 1920x540
  thumbnail:      # 384x216
  screenshot:     # 1920x1080
  screenshots:    # thumbnail: 384x216, screenshot: 1920x1080
    - thumbnail:  001_import_texture.gif
      screenshot: 001_import_texture.gif
    - thumbnail:  002_apply_paper2d_settings.gif
      screenshot: 002_apply_paper2d_settings.gif
    - thumbnail:  003_create_tileset.gif
      screenshot: 003_create_tileset.gif
    - thumbnail:  004_tileset_preview.png
      screenshot: 004_tileset_preview.png
    - thumbnail:  005_tileset_collision_box.png
      screenshot: 005_tileset_collision_box.png
    - thumbnail:  006_tileset_collisions.png
      screenshot: 006_tileset_collisions.png
    - thumbnail:  008_create_tilemap.gif
      screenshot: 008_create_tilemap.gif
    - thumbnail:  009_tilemap_preview.png
      screenshot: 009_tilemap_preview.png
    - thumbnail:  010_tilemap_paint.png
      screenshot: 010_tilemap_paint.png
    - thumbnail:  011_tilemap_settings.png
      screenshot: 011_tilemap_settings.png
    - thumbnail:  012_tilemap_layer_background.png
      screenshot: 012_tilemap_layer_background.png
    - thumbnail:  013_tilemap_layer_background_details.png
      screenshot: 013_tilemap_layer_background_details.png
    - thumbnail:  014_tilemap_layer_ground.png
      screenshot: 014_tilemap_layer_ground.png
    - thumbnail:  015_tilemap_layer_ground_details.png
      screenshot: 015_tilemap_layer_ground_details.png
    - thumbnail:  016_tilemap_layer_foreground.png
      screenshot: 016_tilemap_layer_foreground.png
    - thumbnail:  017_tilemap_clean.png
      screenshot: 017_tilemap_clean.png
    - thumbnail:  018_tilemap_world_2d.png
      screenshot: 018_tilemap_world_2d.png
    - thumbnail:  019_tilemap_world_3d.png
      screenshot: 019_tilemap_world_3d.png
    - thumbnail:  020_move_player.png
      screenshot: 020_move_player.png
    - thumbnail:  021_player_falling.gif
      screenshot: 021_player_falling.gif
    - thumbnail:  022_falling_because_of_foreground_layer.png
      screenshot: 022_falling_because_of_foreground_layer.png
    - thumbnail:  023_move_tilemap_forward_32_units_on_y_axis.png
      screenshot: 023_move_tilemap_forward_32_units_on_y_axis.png
    - thumbnail:  024_player_moving_on_tilemap.gif
      screenshot: 024_player_moving_on_tilemap.gif
    - thumbnail:  025_tilemap_tearing.png
      screenshot: 025_tilemap_tearing.png
    - thumbnail:  026_texture_padding.gif
      screenshot: 026_texture_padding.gif
    - thumbnail:  027_tileset_texture_padding.png
      screenshot: 027_tileset_texture_padding.png
    - thumbnail:  028_player_move_after_texture_padding.gif
      screenshot: 028_player_move_after_texture_padding.gif
buttons:
  source:         https://gitlab.com/Saucyminator/ue4-saucy # external link or link to original source
---

_NOTE: If you've read other tutorials on how to create TileSets and TileMaps in Unreal Engine 4, you can probably skip this one too. :)_  
_NOTE #2: This [post is also available on GitLab](https://gitlab.com/Saucyminator/saucyminator.gitlab.io/blob/master/blog/gamedev/curse-of-undeath/_posts/2015-12-10-unreal-engine-4-tilesets-and-tilemaps.md) if you're having trouble with the font, dark theme or whatnot (just ignore the top part, it's Jekyll information)._

Using TileSets and TileMaps in Unreal Engine 4 you can quickly create 2D maps for your games. This post is maybe more a tutorial of sorts and how I'm gonna use TileSets & TileMaps in my own (prototype) game: Curse of Undeath.

What I _don't_ see in tutorials is any talk about how they go about creating different layers for different uses. I'm gonna try show how _I am_ doing my layering for TileMaps.

Here's the finished example:

![](/assets/blog/gamedev/curse-of-undeath/unreal-engine-4-tilesets-and-tilemaps/028_player_move_after_texture_padding.gif)

### Table of Contents
- [Setting up the texture](#setting-up-the-texture)
- [The TileSet](#the-tileset)
- [Creating the TileMap a.k.a. now the fun begins](#creating-the-tilemap-aka-now-the-fun-begins)
  - [TileMap layers](#tilemap-layers)
    - [Background layer](#background-layer)
    - [Background details layer](#background-details-layer)
    - [Ground layer](#ground-layer)
    - [Ground details layer](#ground-details-layer)
    - [TileMap overview](#tilemap-overview)
- [Adding the TileMap to your level](#adding-the-tilemap-to-your-level)
- [Playing & testing the TileMap!](#playing--testing-the-tilemap)
- [TileMap tearing](#tilemap-tearing)
- [The finished example!](#the-finished-example)


### Setting up the texture
I use [Dirt Platformer Tiles by Lanea Zimmerman](http://opengameart.org/content/dirt-platformer-tiles) for my game. A good thing to know before importing is the tile size of the tileset, as it makes it easier later for the TileSet settings, just keep it in mind (in this example the tile size is 32x32px).

![](/assets/blog/gamedev/curse-of-undeath/unreal-engine-4-tilesets-and-tilemaps/001_import_texture.gif)

Alright, let's start. First we need to import the texture (the tileset) you want to use.
_I'm just dragging my texture into UE4 content browser here._


![](/assets/blog/gamedev/curse-of-undeath/unreal-engine-4-tilesets-and-tilemaps/002_apply_paper2d_settings.gif)

Next we apply a Paper2D texture settings for the newly imported texture, so that we have clean edges and such!

_Right-click on the texture and choose 'Apply Paper2d Texture Settings'_


### The TileSet
![](/assets/blog/gamedev/curse-of-undeath/unreal-engine-4-tilesets-and-tilemaps/003_create_tileset.gif)

Now that's the texture set up, we can finally create a TileSet from the texture.


![](/assets/blog/gamedev/curse-of-undeath/unreal-engine-4-tilesets-and-tilemaps/004_tileset_preview.png)

Let's open the TileSet.

- On the left side you can preview (left-click) the different tiles in your TileSet if they match your current Tile Size setting.
- On the right side you have the Single Tile Editor (for collisions and stuff, more on that in a sec).
  - The panel underneath you can set the Tile Size (this is the tile size I mentioned before). Also here's settings if your tile set has spaces between the tiles themselves. Luckily the tile size of the tileset I'm using is set to 32x32 with no spaces in between, UE4 tile size defaults to 32x32, so I don't have to change anything.


![](/assets/blog/gamedev/curse-of-undeath/unreal-engine-4-tilesets-and-tilemaps/005_tileset_collision_box.png)

Now we need to setup some collision for our tiles so our player can walk on them.

Having the tile selected, and pressing the "Add Box" button at the top, adds a box collision for our tile. In the Single Tile Editor you can change your collision verts if you want (to any shape or size you'd want), but I think you mostly just use the box collision.

_NOTE: If you're unable to see the blue boxes after adding collisions, enable the "Colliding Tiles" by pressing the button at the top left_

![](/assets/blog/gamedev/curse-of-undeath/unreal-engine-4-tilesets-and-tilemaps/006_tileset_collisions.png)

Let's add box collisions to all our tiles we want our player to be able to walk on!

### Creating the TileMap a.k.a. now the fun begins
![](/assets/blog/gamedev/curse-of-undeath/unreal-engine-4-tilesets-and-tilemaps/008_create_tilemap.gif)

Right-click on your TileSet and choose 'Create Tile Map'


![](/assets/blog/gamedev/curse-of-undeath/unreal-engine-4-tilesets-and-tilemaps/009_tilemap_preview.png)

This is what a clean TileMap looks like.

- The right side is where you 'paint' your TileMap.
- The left side is where you choose your tile you wish you 'paint' with, and underneath is the TileMap's settings.


![](/assets/blog/gamedev/curse-of-undeath/unreal-engine-4-tilesets-and-tilemaps/010_tilemap_paint.png)

Selecting a tile allows you to 'paint' it onto the TileMap, but first let's look at the TileMap settings.


![](/assets/blog/gamedev/curse-of-undeath/unreal-engine-4-tilesets-and-tilemaps/011_tilemap_settings.png)

Here's the TileMap settings _I'm_ gonna use for this example, and in my game (except for the width and height, those I expand). Most noticeable settings are the "**Separation Per Layer**" and "**Collision Thickness**".

- "**Separation Per Layer**": It doesn't matter when the camera is orthogonal anyway, and gives me more space between the layers so I can see the collision when dropping the TileMap into the world.
- "**Collision Thickness**": I don't need to use 50 units of collision, so I use a nice low number instead.


#### TileMap layers

##### Background layer
![](/assets/blog/gamedev/curse-of-undeath/unreal-engine-4-tilesets-and-tilemaps/012_tilemap_layer_background.png)

Let's start with the background, pretty straight forward. I use the "Fill" tool to quickly make the background... a background. Done.

_NOTE: I set the "Layer Collides" to false, it's not needed because the background tile doesn't have collision, but I think it's a good habit to get into anyway._


##### Background details layer
![](/assets/blog/gamedev/curse-of-undeath/unreal-engine-4-tilesets-and-tilemaps/013_tilemap_layer_background_details.png)

Here I add all the background details, clouds, stars, moon, and whatnot.

If I'd add them to the background layer there would be holes in the background because of the transparency of the moon and star tiles.

_NOTE: "Layer Collides" is set to false_


##### Ground layer
![](/assets/blog/gamedev/curse-of-undeath/unreal-engine-4-tilesets-and-tilemaps/014_tilemap_layer_ground.png)

A simple Ground layer for the player to walk on. This is the only layer the player interact with.


##### Ground details layer
![](/assets/blog/gamedev/curse-of-undeath/unreal-engine-4-tilesets-and-tilemaps/015_tilemap_layer_ground_details.png)

Ground Details layer that's behind the Ground layer (underneath the ground layer in the tile layer list). Why I made the Ground layer first is mostly because I needed to see where the ground went first.

Technically speaking I could add another Grounds Details layer that display in front of the player (above the 'ground' layer in the tile layer list). Example: The player's feet would be displayed behind some grass tiles.

But I think my next layer would solve any of that anyway.

_NOTE: "Layer Collides" is set to false_


##### Foreground layer
![](/assets/blog/gamedev/curse-of-undeath/unreal-engine-4-tilesets-and-tilemaps/016_tilemap_layer_foreground.png)

A layer that would be display in front of the player (nearest the camera).

_NOTE: "Layer Collides" is set to false_


##### TileMap overview
![](/assets/blog/gamedev/curse-of-undeath/unreal-engine-4-tilesets-and-tilemaps/017_tilemap_clean.png)

Here's what it looks without the tile grid help!


### Adding the TileMap to your level
Adding the TileMap to the level is just like any actor you have in your content browser, drag and drop!

_NOTE: I set the TileMap at 0 units on the Y axis._

Here's what it looks like in 2D:
![](/assets/blog/gamedev/curse-of-undeath/unreal-engine-4-tilesets-and-tilemaps/018_tilemap_world_2d.png)

And in 3D:
![](/assets/blog/gamedev/curse-of-undeath/unreal-engine-4-tilesets-and-tilemaps/019_tilemap_world_3d.png)

_Here you can see which layer has collision and which that does not._

### Playing & testing the TileMap!
Let's start testing it out. First we need to add/move our player spawn to where the TileMap is.

![](/assets/blog/gamedev/curse-of-undeath/unreal-engine-4-tilesets-and-tilemaps/020_move_player.png)

_Don't mind the "BAD size" error on the player spawn, it doesn't play nice with TileMaps._

_NOTE: I set the player spawn at 0 units on the Y axis._


![](/assets/blog/gamedev/curse-of-undeath/unreal-engine-4-tilesets-and-tilemaps/021_player_falling.gif)

...Ooops! Why are we falling through space & time? Simple: the player is currently on the "foreground" layer, which has no collision.


![](/assets/blog/gamedev/curse-of-undeath/unreal-engine-4-tilesets-and-tilemaps/022_falling_because_of_foreground_layer.png)

It's kinda hard to see, but here's the problem: Because of the way the TileMaps work and where they have their point-of-origin in the level, the top layer is the point-of-origin.

Because our 'foreground' layer doesn't have any collision the player falls through.

Not much we can do to the TileMap itself (other than removing the 'foreground' layer), so instead we move the TileMap **32 units on the Y axis**.


![](/assets/blog/gamedev/curse-of-undeath/unreal-engine-4-tilesets-and-tilemaps/023_move_tilemap_forward_32_units_on_y_axis.png)

After we moved the TileMap 32 units on the Y axis, we can see the player and the ground layer matches their Y axis.


### TileMap tearing
![](/assets/blog/gamedev/curse-of-undeath/unreal-engine-4-tilesets-and-tilemaps/024_player_moving_on_tilemap.gif)

Alright, after fixing the falling through space & time part, we play again to check if it's working. It is! But hold on, do _you_ see the tearing in this gif?


![](/assets/blog/gamedev/curse-of-undeath/unreal-engine-4-tilesets-and-tilemaps/025_tilemap_tearing.png)

Here's a close up image on the tearing. It's pretty nasty. If you're like me, you don't want that in your game.

So how do we fix it? Easy.


![](/assets/blog/gamedev/curse-of-undeath/unreal-engine-4-tilesets-and-tilemaps/026_texture_padding.gif)

We simply go back to our TileSet, right-click on it and choose 'Condition Tile Sheet Texture'.

What it does is it adds padding the each tile so tearing like we previously saw won't happen! (isn't UE4 awesome?!)

Here's what the padded texture looks like (compare this to the [TileSet without padding](/assets/blog/gamedev/curse-of-undeath/unreal-engine-4-tilesets-and-tilemaps/004_tileset_preview.png)):

![](/assets/blog/gamedev/curse-of-undeath/unreal-engine-4-tilesets-and-tilemaps/027_tileset_texture_padding.png)

_NOTE: I believe you can remove the original texture after making a padded one, I like to keep it if I ever need it again for reference or something._

### The finished example!
![](/assets/blog/gamedev/curse-of-undeath/unreal-engine-4-tilesets-and-tilemaps/028_player_move_after_texture_padding.gif)

This is the first tutorial I've made, so any feedback is more than welcome! I hope I've made myself clear, and the instructions were easy to follow.

This wouldn't be possible without:

- [Epic for Unreal Engine 4](https://www.unrealengine.com)
- [Calaelen for his blog posts on UE4 platformer](http://nerd-time.com)
- [Lanea Zimmerman for the Dirt Platformer Tiles](http://opengameart.org/content/dirt-platformer-tiles)
- [Universal LPC Sprite Sheet for the character and it's animations](http://gaurav.munjal.us/Universal-LPC-Spritesheet-Character-Generator/)

Thanks!
