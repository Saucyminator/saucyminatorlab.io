---
layout:           post
title:            'First post! Unreal Engine 4 prototype game: Curse of Undeath'
date:             2015-12-06 03:22
date_updated:     
tags:             [GameDev, Games, Curse of Undeath, Prototype, Unreal Engine 4]
summary:          First post about a prototype game I'm making in Unreal Engine 4!
images:
  banner:         banner.png # 1920x540
  thumbnail:      # 384x216
  screenshot:     # 1920x1080
  screenshots:    # thumbnail: 384x216, screenshot: 1920x1080
    - thumbnail:  idle.gif
      screenshot: idle.gif
    - thumbnail:  walk.gif
      screenshot: walk.gif
    - thumbnail:  attack.gif
      screenshot: attack.gif
buttons:
  source:         https://gitlab.com/Saucyminator/ue4-saucy # external link or link to original source
---

First blog post I've ever made. Been waiting a bit too long for this one to pop-up.

I'll be showing off my current prototype game I'm working on in [Unreal Engine 4 (UE4)](https://www.unrealengine.com). I've been using UE4 on and off since March 2015. Trying out different ideas and such (you can find more prototype games by me [on GitLab](https://gitlab.com/Saucyminator/ue4-saucy)).

I have a few things I'd like to create, but recently reading [Calaelen's GameDev Journey](http://nerd-time.com/ue4-2d-platformer-paper2d/) and watching [Extra Credits: Minimum Viable Product video](https://www.youtube.com/watch?v=UvCri1tqIxQ) I felt like I had to complete at least one game in my lifetime. And hell be damned I'm gonna try!

In other news, feel free to leave any comments about the website, the game I'm currently prototyping. Any feedback about anything helps, you can't learn if you don't learn from your mistakes!

Below are a few gif's of me trying out the idle, walking and attacking animations of my (skeleton) character!

(P.S. You find more information about the game via the "Source" button, which takes you to the source code on [GitLab](https://gitlab.com). I'm gonna put all my game ideas in the Readme file.)
